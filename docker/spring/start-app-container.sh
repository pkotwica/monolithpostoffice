#!/bin/bash

IMAGE_NAME="monolith-app:latest"
CONTAINER_NAME="monolith-app"
JAR_FILENAME="monolith-0.0.1-SNAPSHOT.jar"
JAR_PATH="../../target/$JAR_FILENAME"
TEMP_PATH="./tmp"
TEMP_JAR_FILE="$TEMP_PATH/$JAR_FILENAME"

build_flag=0
stop_container=0

while getopts ":bs" option; do
  case $option in
  b) build_flag=1 ;;
  s) stop_container=1 ;;
  *) ;;
  esac
done

if [ $stop_container -eq 1 ]; then
  printf "\n\nSTOPPING CONTAINER: $CONTAINER_NAME\n"
  docker stop $CONTAINER_NAME
else
  printf "\n\nTo stop old container use (-s) option.\n"
fi

printf "\n\ndocker container prune\n"
docker container prune -f

if [ $build_flag -eq 1 ]; then
  (
    cd ../../
    mvn clean package -DskipTests=true
  )

  printf "\n\nBUILDING IMAGE: $IMAGE_NAME\n"
  mkdir $TEMP_PATH
  cp $JAR_PATH $TEMP_JAR_FILE
  docker image prune -f
  docker build --build-arg JAR_FILE=$TEMP_JAR_FILE -t $IMAGE_NAME --rm=true .
  rm -r $TEMP_PATH
else
  printf "\n\nContainer starts without building image! (-b to build)\n\n"
fi

docker run --name $CONTAINER_NAME -dp 8090:8090 $IMAGE_NAME
