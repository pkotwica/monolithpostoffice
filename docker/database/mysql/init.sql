CREATE SCHEMA post_office;
-- CREATE USER 'monolith' IDENTIFIED BY 'monolith';
GRANT ALL PRIVILEGES ON post_office.* TO 'monolith'@'%' IDENTIFIED BY 'monolith';
GRANT ALL PRIVILEGES ON post_office.* TO 'monolith'@'localhost' IDENTIFIED BY 'monolith';