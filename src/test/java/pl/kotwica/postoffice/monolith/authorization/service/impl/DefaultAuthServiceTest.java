package pl.kotwica.postoffice.monolith.authorization.service.impl;

import org.assertj.core.data.Offset;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;
import pl.kotwica.postoffice.monolith.authorization.model.LoginBody;
import pl.kotwica.postoffice.monolith.authorization.repo.AuthTokenRepo;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAuthServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthTokenRepo tokenRepository;
    @Mock
    private PasswordService passwordEncoder;

    @InjectMocks
    private DefaultAuthService defaultAuthService;

    private final static String EXAMPLE_PASSWORD = "example_password";
    private final static LoginBody LOGIN_BODY = new LoginBody(SIMPLE_EMAIL, EXAMPLE_PASSWORD);

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void tokenGeneratedIfUserExistsAndPasswordMatched() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(passwordEncoder.matches(EXAMPLE_PASSWORD, SIMPLE_PASS)).thenReturn(true);

        //when
        Optional<AuthToken> generateTokenResult = defaultAuthService.generateAuthToken(LOGIN_BODY);

        //then
        assertThat(generateTokenResult.isPresent()).isTrue();
    }

    @Test
    public void tokenGeneratedWith30minutesExpirationTime() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(passwordEncoder.matches(EXAMPLE_PASSWORD, SIMPLE_PASS)).thenReturn(true);

        //when
        Optional<AuthToken> generateTokenResult = defaultAuthService.generateAuthToken(LOGIN_BODY);

        //then
        assertThat(generateTokenResult.get().getExpirationDate().getMinute()).isCloseTo(LocalDateTime.now().plusMinutes(30).getMinute(), Offset.offset(1));
    }

    @Test
    public void tokenNotGeneratedIfUserNotExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.empty());

        //when
        Optional<AuthToken> generateTokenResult = defaultAuthService.generateAuthToken(LOGIN_BODY);

        //then
        assertThat(generateTokenResult.isPresent()).isFalse();
    }

    @Test
    public void tokenNotGeneratedIfPasswordNotMatched() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(passwordEncoder.matches(EXAMPLE_PASSWORD, SIMPLE_PASS)).thenReturn(false);

        //when
        Optional<AuthToken> generateTokenResult = defaultAuthService.generateAuthToken(LOGIN_BODY);

        //then
        assertThat(generateTokenResult.isPresent()).isFalse();
    }
}