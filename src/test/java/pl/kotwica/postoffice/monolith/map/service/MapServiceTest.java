package pl.kotwica.postoffice.monolith.map.service;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.map.model.*;
import pl.kotwica.postoffice.monolith.map.repo.DistanceRepository;
import pl.kotwica.postoffice.monolith.map.repo.RoadRepository;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.SIMPLE_ID_ONE;

@RunWith(MockitoJUnitRunner.class)
public class MapServiceTest {

    @Mock
    private DistanceRepository distanceRepository;
    @Mock
    private RoadRepository roadRepository;

    @InjectMocks
    private MapService mapService;


    private static final Integer VALID_NUMBER = 1;
    private static final Integer MIN_NUMBER = 1;
    private static final Integer MAX_NUMBER = 10;
    private static final Integer NOT_VALID_NUMBER = 11;
    private static final String ROAD_NAME = "Road";
    private static final String POSTCODE_CODE_ONE = "WROC_1";
    private static final String POSTCODE_CODE_TWO = "WROC_2";
    private static final String CITY_NAME = "City";
    private static final String NOT_VALID_CITY_NAME = "Not valid city name";
    private static final String COUNTRY_NAME = "Country";
    private static final String NOT_VALID_COUNTRY_NAME = "Not valid country";

    private static final Integer DISTANCE_VALUE = 101;
    private static final Integer ZERO_DISTANCE_VALUE = 0;

    private static final Postcode POSTCODE_ONE = new Postcode(SIMPLE_ID_ONE, POSTCODE_CODE_ONE, null);
    private static final Postcode POSTCODE_TWO = new Postcode(SIMPLE_ID_ONE, POSTCODE_CODE_TWO, null);
    private static final Distance DISTANCE = new Distance(SIMPLE_ID_ONE, POSTCODE_ONE, POSTCODE_TWO, DISTANCE_VALUE);

    private static final Country COUNTRY = new Country(SIMPLE_ID_ONE, COUNTRY_NAME);
    private static final City CITY = new City(SIMPLE_ID_ONE, CITY_NAME, COUNTRY);
    private static final Postcode POSTCODE = new Postcode(SIMPLE_ID_ONE, POSTCODE_CODE_ONE, CITY);
    private static final Road ROAD = new Road(SIMPLE_ID_ONE, ROAD_NAME, MIN_NUMBER, MAX_NUMBER, POSTCODE);

    private static final AddressExistenceForm ADDRESS_TEMPLATE = new AddressExistenceForm(COUNTRY_NAME, CITY_NAME, POSTCODE_CODE_ONE, ROAD_NAME, VALID_NUMBER);

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void exactDistanceOfValidPostcodes() {
        //given
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_ONE, POSTCODE_CODE_TWO)).thenReturn(Optional.of(DISTANCE));
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_TWO, POSTCODE_CODE_ONE)).thenReturn(Optional.empty());

        //when
        Integer distanceValueMethodResult = mapService.getDistance(POSTCODE_CODE_ONE, POSTCODE_CODE_TWO);

        //then
        assertThat(distanceValueMethodResult).isEqualTo(DISTANCE_VALUE);
    }

    @Test
    public void exactDistanceOfValidSwappedPostcodes() {
        //given
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_ONE, POSTCODE_CODE_TWO)).thenReturn(Optional.empty());
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_TWO, POSTCODE_CODE_ONE)).thenReturn(Optional.of(DISTANCE));

        //when
        Integer distanceValueMethodResult = mapService.getDistance(POSTCODE_CODE_TWO, POSTCODE_CODE_ONE);

        //then
        assertThat(distanceValueMethodResult).isEqualTo(DISTANCE_VALUE);
    }

    @Test
    public void zeroDistanceOfNonValidPostcodes() {
        //given
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_ONE, POSTCODE_CODE_TWO)).thenReturn(Optional.empty());
        when(distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(POSTCODE_CODE_TWO, POSTCODE_CODE_ONE)).thenReturn(Optional.empty());

        //when
        Integer distanceValueMethodResult = mapService.getDistance(POSTCODE_CODE_TWO, POSTCODE_CODE_ONE);

        //then
        assertThat(distanceValueMethodResult).isEqualTo(ZERO_DISTANCE_VALUE);
    }

    @Test
    public void trueIfAddressExist() {
        //given
        when(roadRepository.findFirstByNameAndPostcode_Code(ROAD_NAME, POSTCODE_CODE_ONE)).thenReturn(Optional.of(ROAD));

        //when
        boolean addressExistenceMethodResult = mapService.checkAddressExistence(ADDRESS_TEMPLATE);

        //then
        assertThat(addressExistenceMethodResult).isTrue();
    }

    @Test
    public void falseIfRoadNumberNotInSet() {
        //given
        when(roadRepository.findFirstByNameAndPostcode_Code(ROAD_NAME, POSTCODE_CODE_ONE)).thenReturn(Optional.of(ROAD));
        AddressExistenceForm addressWithWrongRoadNumber = new AddressExistenceForm(COUNTRY_NAME, CITY_NAME, POSTCODE_CODE_ONE, ROAD_NAME, NOT_VALID_NUMBER);

        //when
        boolean addressExistenceMethodResult = mapService.checkAddressExistence(addressWithWrongRoadNumber);

        //then
        assertThat(addressExistenceMethodResult).isFalse();
    }

    @Test
    public void falseIfRoadWithPostcodeNotExists() {
        //given
        when(roadRepository.findFirstByNameAndPostcode_Code(ROAD_NAME, POSTCODE_CODE_ONE)).thenReturn(Optional.empty());

        //when
        boolean addressExistenceMethodResult = mapService.checkAddressExistence(ADDRESS_TEMPLATE);

        //then
        assertThat(addressExistenceMethodResult).isFalse();
    }

    @Test
    public void falseIfPostcodeNotBelongsToCity() {
        //given
        when(roadRepository.findFirstByNameAndPostcode_Code(ROAD_NAME, POSTCODE_CODE_ONE)).thenReturn(Optional.of(ROAD));
        AddressExistenceForm addressWithWrongRoadNumber = new AddressExistenceForm(COUNTRY_NAME, NOT_VALID_CITY_NAME, POSTCODE_CODE_ONE, ROAD_NAME, VALID_NUMBER);

        //when
        boolean addressExistenceMethodResult = mapService.checkAddressExistence(addressWithWrongRoadNumber);

        //then
        assertThat(addressExistenceMethodResult).isFalse();
    }

    @Test
    public void falseIfCityNotBelongsToCountry() {
        //given
        when(roadRepository.findFirstByNameAndPostcode_Code(ROAD_NAME, POSTCODE_CODE_ONE)).thenReturn(Optional.of(ROAD));
        AddressExistenceForm addressWithWrongRoadNumber = new AddressExistenceForm(NOT_VALID_COUNTRY_NAME, CITY_NAME, POSTCODE_CODE_ONE, ROAD_NAME, VALID_NUMBER);

        //when
        boolean addressExistenceMethodResult = mapService.checkAddressExistence(addressWithWrongRoadNumber);

        //then
        assertThat(addressExistenceMethodResult).isFalse();
    }
}