package pl.kotwica.postoffice.monolith.addressBook.service;


import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.addressBook.model.AddressDto;
import pl.kotwica.postoffice.monolith.addressBook.repo.AddressBookRepository;
import pl.kotwica.postoffice.monolith.user.service.UtilUserService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class AddressBookServiceTest {

    @Mock
    private AddressBookRepository addressBookRepository;
    @Mock
    private UtilUserService utilUserService;

    @InjectMocks
    private AddressBookService addressBookService;

    private static final Address EXAMPLE_ADDRESS = new Address(SIMPLE_ID_ONE, SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, ADDRESS_COUNTRY, ADDRESS_CITY, ADDRESS_POSTCODE, ADDRESS_ROAD, ADDRESS_NUMBER, SIMPLE_USER_ONE);
    private static final List<Address> REPOSITORY_ADDRESSES_BOOK = Collections.singletonList(EXAMPLE_ADDRESS);

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void listOfAddressesDTO() {
        //given
        when(addressBookRepository.findAllByUser_Email(any(String.class))).thenReturn(REPOSITORY_ADDRESSES_BOOK);
        when(utilUserService.getLoggedUserEmail()).thenReturn(SIMPLE_EMAIL);

        //when
        List<AddressDto> addresses = addressBookService.getUserAddresses();

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(addresses.stream().map(AddressDto::getId)).containsExactly(SIMPLE_ID_ONE);
        softly.assertThat(addresses.stream().map(AddressDto::getFirstName)).containsExactly(SIMPLE_FIRST_NAME);
        softly.assertThat(addresses.stream().map(AddressDto::getLastName)).containsExactly(SIMPLE_LAST_NAME);
        softly.assertThat(addresses.stream().map(AddressDto::getCountry)).containsExactly(ADDRESS_COUNTRY);
        softly.assertThat(addresses.stream().map(AddressDto::getCity)).containsExactly(ADDRESS_CITY);
        softly.assertThat(addresses.stream().map(AddressDto::getPostcode)).containsExactly(ADDRESS_POSTCODE);
        softly.assertThat(addresses.stream().map(AddressDto::getRoad)).containsExactly(ADDRESS_ROAD);
        softly.assertThat(addresses.stream().map(AddressDto::getNumber)).containsExactly(ADDRESS_NUMBER);
        softly.assertAll();
    }

    @Test
    public void emptyListIfNoAddressesInBook() {
        //given
        when(addressBookRepository.findAllByUser_Email(any(String.class))).thenReturn(Collections.emptyList());
        when(utilUserService.getLoggedUserEmail()).thenReturn(SIMPLE_EMAIL);

        //when
        List<AddressDto> addresses = addressBookService.getUserAddresses();

        //then
        assertThat(addresses).isEmpty();
    }

    @Test
    public void addressSavedIfUserLogged() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));

        //when
        Boolean addMethodResult = addressBookService.add(SHIPMENT_FORM_ADDRESS);

        //then
        assertThat(addMethodResult).isTrue();
        verify(addressBookRepository, times(1)).save(any(Address.class));
    }

    @Test
    public void addressNotSaveIfUserNotLoggedIn() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());

        //when
        Boolean addMethodResult = addressBookService.add(SHIPMENT_FORM_ADDRESS);

        //then
        assertThat(addMethodResult).isFalse();
        verify(addressBookRepository, times(0)).save(any(Address.class));
    }

    @Test
    public void removeAddressWhenBelongsToUserAndUserLoggedIn() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(addressBookRepository.existsAddressByUserAndId(SIMPLE_USER_ONE, SIMPLE_ID_ONE)).thenReturn(true);

        //when
        addressBookService.remove(SIMPLE_ID_ONE);

        //then
        verify(addressBookRepository, times(1)).deleteById(SIMPLE_ID_ONE);
    }

    @Test
    public void notRemoveAddressWhenUserNotLoggedIn() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(addressBookRepository.existsAddressByUserAndId(SIMPLE_USER_ONE, SIMPLE_ID_TWO)).thenReturn(false);

        //when
        addressBookService.remove(SIMPLE_ID_TWO);

        //then
        verify(addressBookRepository, times(0)).deleteById(any(Integer.class));
    }

    @Test
    public void notRemoveAddressWhenNotBelongsToUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());

        //when
        addressBookService.remove(SIMPLE_ID_ONE);

        //then
        verify(addressBookRepository, times(0)).deleteById(any(Integer.class));
    }
}