package pl.kotwica.postoffice.monolith.addressBook.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.testUtils.CommonControllerIntegrationTest;
import pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData;
import pl.kotwica.postoffice.monolith.user.model.User;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData.LOGGED_USER_EMAIL;

class AddressBookControllerIntegrationTest extends CommonControllerIntegrationTest {

    private static User standardUser;
    private static Address addressBook;

    public static final String VALID_ADDRESS_FORM = "{\n" +
            "    \"firstName\": \"Jan\",\n" +
            "    \"lastName\": \"Kowalski\",\n" +
            "    \"phoneNumber\": \"191919191\",\n" +
            "    \"email\": \"email@courierpackage.com\",\n" +
            "    \"country\": \"Polska\",\n" +
            "    \"city\": \"Wroclaw\",\n" +
            "    \"postcode\": \"WROC-1\",\n" +
            "    \"road\": \"Kamienna\" ,\n" +
            "    \"number\": 1\n" +
            "}";

    public static final String INCOMPLETE_ADDRESS_FORM = "{\n" +
            "    \"firstName\": \"\",\n" +
            "    \"lastName\": \"Kowalski\",\n" +
            "    \"phoneNumber\": \"191919191\",\n" +
            "    \"email\": \"email@courierpackage.com\",\n" +
            "    \"country\": \"Polska\",\n" +
            "    \"city\": \"Wroclaw\",\n" +
            "    \"postcode\": \"WROC-1\",\n" +
            "    \"road\": \"Kamienna\" ,\n" +
            "    \"number\": 1\n" +
            "}";

    public static final String NONEXISTENT_ADDRESS_FORM = "{\n" +
            "    \"firstName\": \"Jan\",\n" +
            "    \"lastName\": \"Kowalski\",\n" +
            "    \"phoneNumber\": \"191919191\",\n" +
            "    \"email\": \"email@courierpackage.com\",\n" +
            "    \"country\": \"Polska\",\n" +
            "    \"city\": \"Warszawa\",\n" +
            "    \"postcode\": \"WROC-1\",\n" +
            "    \"road\": \"Kamienna\" ,\n" +
            "    \"number\": 1\n" +
            "}";

    @BeforeAll
    public static void init(@Autowired IntegrationTestData integrationTestData) {
        standardUser = integrationTestData.addStandardUser();
        addressBook = integrationTestData.addAddressBookExampleAddress(standardUser);
        integrationTestData.addMapAddresses();
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void getAddressesCauseOkStatusWithListOfAddresses() throws Exception {
        performProtectedGetRequest("/address-book/get-addresses", logStandardUser(), "")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", hasItem(addressBook.getId())))
                .andExpect(jsonPath("$[*].firstName", hasItem(addressBook.getFirstName())))
                .andExpect(jsonPath("$[*].lastName", hasItem(addressBook.getLastName())))
                .andExpect(jsonPath("$[*].country", hasItem(addressBook.getCountry())))
                .andExpect(jsonPath("$[*].city", hasItem(addressBook.getCity())))
                .andExpect(jsonPath("$[*].postcode", hasItem(addressBook.getPostcode())))
                .andExpect(jsonPath("$[*].road", hasItem(addressBook.getRoad())))
                .andExpect(jsonPath("$[*].number", hasItem(addressBook.getNumber())));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void successfullyAddedAddressCauseOkStatusWithSuccessJson() throws Exception {
        performProtectedPostRequest("/address-book/add", logStandardUser(), VALID_ADDRESS_FORM)
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": true, " +
                                "\"message\": \"Address added\", " +
                                "\"errors\": null}"
                ));
    }

    @Test
    void formErrorsAddingAddressCauseBadRequestWithNoSuccessJson() throws Exception {
        performProtectedPostRequest("/address-book/add", logStandardUser(), INCOMPLETE_ADDRESS_FORM)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Not valid form"))
                .andExpect(jsonPath("$.errors[0].key").value("shipmentFormAddress"))
                .andExpect(jsonPath("$.errors[0].value").value("must not be empty"));
    }

    @Test
    void addressNotExistsAddingAddressCauseBadRequestWithNoSuccessJson() throws Exception {
        performProtectedPostRequest("/address-book/add", logStandardUser(), NONEXISTENT_ADDRESS_FORM)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Not valid form"))
                .andExpect(jsonPath("$.errors[0].key").value("shipmentFormAddress"))
                .andExpect(jsonPath("$.errors[0].value").value("Given address does not exist."));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void deletionAddressCauseOkStatus() throws Exception {
        performProtectedDeleteRequest("/address-book/remove?addressId=" + addressBook.getId(), logStandardUser(), "")
                .andExpect(status().isOk());

        addressBook = integrationTestData.addAddressBookExampleAddress(standardUser);
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void deleteNotExistingAddressCauseOkStatus() throws Exception {
        performProtectedDeleteRequest("/address-book/remove?addressId=" + (addressBook.getId() + 100), logStandardUser(), "")
                .andExpect(status().isOk());
    }
}