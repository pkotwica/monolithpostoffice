package pl.kotwica.postoffice.monolith.deliverer.courier.service;

import lombok.AllArgsConstructor;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.SpareCourier;

@AllArgsConstructor
public class SpareCourierImpl implements SpareCourier {

    private final Integer id;
    private final Integer capacity;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public Integer getCapacity() {
        return capacity;
    }
}
