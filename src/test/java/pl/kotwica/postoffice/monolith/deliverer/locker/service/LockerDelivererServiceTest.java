package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.*;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerBoxRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerRepository;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.packages.model.PackageFeedback;
import pl.kotwica.postoffice.monolith.packages.model.PackageInfo;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class LockerDelivererServiceTest {
    @Mock
    private LockerBoxRepository lockerBoxRepository;
    @Mock
    private LockerPackageRepository lockerPackageRepository;
    @Mock
    private LockerRepository lockerRepository;
    @Mock
    private LockerPriceService lockerPriceService;
    @Mock
    private LockerTimeService lockerTimeService;

    @InjectMocks
    private LockerDelivererService lockerDelivererService;

    private final static String LOCKER_NAME = "Paczkomat WROC1";
    private final static Locker LOCKER = new Locker(SIMPLE_ID_ONE, LOCKER_CODE, LOCKER_NAME, ADDRESS_COUNTRY, ADDRESS_CITY, 1, ADDRESS_ROAD, "1", null);
    private final static Locker LOCKER_DESTINATION = new Locker(SIMPLE_ID_ONE, LOCKER_CODE_DESTINATION, LOCKER_NAME, ADDRESS_COUNTRY, ADDRESS_CITY_DESTINATION, 1, ADDRESS_ROAD, "1", null);
    private final static LockerBox LOCKER_BOX = new LockerBox(SIMPLE_ID_ONE, false, LOCKER);

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() throws Exception {
        LOCKER_PACKAGE_INTRODUCED.setStatus(LockerPackageStatus.INTRODUCED.name());
        LOCKER_PACKAGE_CONFIRMED.setStatus(LockerPackageStatus.CONFIRMED.name());
    }

    @Test
    public void infoDataIsNoEmpty() {
        //when
        DelivererInfoData infoData = lockerDelivererService.getInfoData();

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(infoData).isNotNull();
        softly.assertThat(infoData.isActive()).isNotNull();
        softly.assertThat(infoData.getDelivererID()).isNotNull();
        softly.assertThat(infoData.getDelivererName()).isNotNull();
        softly.assertThat(infoData.getDelivererName()).isNotEmpty();
        softly.assertAll();
    }

    @Test
    public void getExactlyPackagesInfoOfOwner() {
        //given
        when(lockerPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(LOCKER_PACKAGE_INTRODUCED));

        //when
        List<PackageInfo> usersPackages = lockerDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        assertThat(usersPackages.stream().map(PackageInfo::getId)).containsExactly(LOCKER_PACKAGE_INTRODUCED.getId());
    }

    @Test
    public void packageInfoInListOfPackagesIsNotEmpty() {
        //given
        when(lockerPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(LOCKER_PACKAGE_INTRODUCED));

        //when
        List<PackageInfo> usersPackages = lockerDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(usersPackages.get(0).getId()).isNotNull();
        softly.assertThat(usersPackages.get(0).getSourceAddress()).isNotNull();
        softly.assertThat(usersPackages.get(0).getDestinationAddress()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPackageSize()).isNotNull();
        softly.assertThat(usersPackages.get(0).getSendingDate()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPredictionDeliveryDate()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPrice()).isNotNull();
        softly.assertThat(usersPackages.get(0).getAdditionalDelivererInfo()).isNotNull();
        softly.assertAll();
    }

    @Test
    public void emptyListOfPackagesInfoIfOwnerHasNoPackages() {
        //given
        when(lockerPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.emptyList());

        //when
        List<PackageInfo> usersPackages = lockerDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        assertThat(usersPackages).isEmpty();
    }

    @Test
    public void packageInfoForSpecifiedPackageIdIsNotEmpty() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));

        //when
        PackageFeedback packageFeedback = lockerDelivererService.getPackageInfo(SIMPLE_ID_ONE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(packageFeedback.isSuccess()).isTrue();
        softly.assertThat(packageFeedback.getPackageInfo()).isNotNull();
        softly.assertAll();
    }

    @Test
    public void errorPackageInfoIfSpecifiedIdIsWrong() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.empty());

        //when
        PackageFeedback packageFeedback = lockerDelivererService.getPackageInfo(SIMPLE_ID_ONE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void errorIfNotSufficientSourceLockersPropositions() {
        //given
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.emptyList());
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_DESTINATION_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));

        //when
        PackageFeedback packageFeedback = lockerDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void errorIfNotSufficientDestinationLockersPropositions() {
        //given
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_DESTINATION_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.emptyList());

        //when
        PackageFeedback packageFeedback = lockerDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void successPackageFeedbackIsNotEmpty() {
        //given
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_DESTINATION_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));
        when(lockerPriceService.calculatePriceForPackage(MAIN_PACKAGE)).thenReturn(BigDecimal.TEN);
        when(lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE)).thenReturn(SIMPLE_DATE);
        when(lockerPackageRepository.save(any(LockerPackage.class))).thenReturn(LOCKER_PACKAGE_INTRODUCED);

        //when
        PackageFeedback packageFeedback = lockerDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(packageFeedback.isSuccess()).isTrue();
        softly.assertThat(packageFeedback.getPackageInfo()).isNotNull();
        softly.assertThat(packageFeedback.getPackageInfo().getPrice()).isEqualByComparingTo(BigDecimal.TEN);
        softly.assertThat(packageFeedback.getPackageInfo().getPredictionDeliveryDate()).isEqualTo(SIMPLE_DATE);
        softly.assertAll();
    }

    @Test
    public void successPackageFeedbackContainsNotEmptyListsOfLockersPropositions() {
        //given
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));
        when(lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(SHIPMENT_FORM_DESTINATION_ADDRESS.getCity().toLowerCase())).thenReturn(Collections.singletonList(new SpareLockerImpl(LOCKER)));
        when(lockerPriceService.calculatePriceForPackage(MAIN_PACKAGE)).thenReturn(BigDecimal.TEN);
        when(lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE)).thenReturn(SIMPLE_DATE);
        when(lockerPackageRepository.save(any(LockerPackage.class))).thenReturn(LOCKER_PACKAGE_INTRODUCED);

        //when
        PackageFeedback packageFeedback = lockerDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        AdditionalLockerDelivererInfo additionalDelivererInfo = (AdditionalLockerDelivererInfo) packageFeedback.getPackageInfo().getAdditionalDelivererInfo();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(additionalDelivererInfo.getSourceLockers().stream().map(LockerDTO::getCode)).containsExactly(LOCKER.getCode());
        softly.assertThat(additionalDelivererInfo.getDestinationLockers().stream().map(LockerDTO::getCode)).containsExactly(LOCKER.getCode());
        softly.assertAll();
    }

    @Test
    public void confirmedPackageIsSaved() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));
        when(lockerRepository.findByCode(LOCKER_CODE)).thenReturn(Optional.of(LOCKER));
        when(lockerRepository.findByCode(LOCKER_CODE_DESTINATION)).thenReturn(Optional.of(LOCKER_DESTINATION));
        when(lockerBoxRepository.findFirstByOccupiedIsFalseAndLocker_Id(any(Integer.class))).thenReturn(Optional.of(LOCKER_BOX));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(1)).save(any(LockerPackage.class));
        assertThat(confirmResult).isTrue();
        assertThat(LOCKER_PACKAGE_INTRODUCED.getStatus()).isEqualTo(LockerPackageStatus.CONFIRMED.name());
    }

    @Test
    public void declinePackageIsSave() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(DECLINE_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(1)).save(any(LockerPackage.class));
        assertThat(confirmResult).isTrue();
        assertThat(LOCKER_PACKAGE_INTRODUCED.getStatus()).isEqualTo(LockerPackageStatus.DECLINED.name());
    }

    @Test
    public void waitingPackageIsSavedIfNoSufficientLockerBoxes() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));
        when(lockerRepository.findByCode(LOCKER_CODE)).thenReturn(Optional.of(LOCKER));
        when(lockerRepository.findByCode(LOCKER_CODE_DESTINATION)).thenReturn(Optional.of(LOCKER_DESTINATION));
        when(lockerBoxRepository.findFirstByOccupiedIsFalseAndLocker_Id(any(Integer.class))).thenReturn(Optional.empty());

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(1)).save(any(LockerPackage.class));
        assertThat(confirmResult).isTrue();
        assertThat(LOCKER_PACKAGE_INTRODUCED.getStatus()).isEqualTo(LockerPackageStatus.WAITING.name());
    }

    @Test
    public void errorIfPackageDuringConfirmationHasOtherStatusThatIntroduced() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_CONFIRMED));
        when(lockerRepository.findByCode(LOCKER_CODE)).thenReturn(Optional.of(LOCKER));
        when(lockerRepository.findByCode(LOCKER_CODE_DESTINATION)).thenReturn(Optional.of(LOCKER_DESTINATION));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(0)).save(any(LockerPackage.class));
        assertThat(confirmResult).isFalse();
    }

    @Test
    public void errorIfChosenLockerNotBelongsToAddressCity() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));
        when(lockerRepository.findByCode(any(String.class))).thenReturn(Optional.of(LOCKER));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(0)).save(any(LockerPackage.class));
        assertThat(confirmResult).isFalse();
    }

    @Test
    public void errorIfPackageNotExists() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.empty());
        when(lockerRepository.findByCode(LOCKER_CODE)).thenReturn(Optional.of(LOCKER));
        when(lockerRepository.findByCode(LOCKER_CODE_DESTINATION)).thenReturn(Optional.of(LOCKER_DESTINATION));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(0)).save(any(LockerPackage.class));
        assertThat(confirmResult).isFalse();
    }

    @Test
    public void errorIfChosenLockersNotExists() {
        //given
        when(lockerPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(LOCKER_PACKAGE_INTRODUCED));
        when(lockerRepository.findByCode(LOCKER_CODE)).thenReturn(Optional.empty());
        when(lockerRepository.findByCode(LOCKER_CODE_DESTINATION)).thenReturn(Optional.of(LOCKER_DESTINATION));

        //when
        boolean confirmResult = lockerDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(lockerPackageRepository, times(0)).save(any(LockerPackage.class));
        assertThat(confirmResult).isFalse();
    }
}