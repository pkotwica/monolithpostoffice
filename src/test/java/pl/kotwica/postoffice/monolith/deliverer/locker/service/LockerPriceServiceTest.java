package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.junit.jupiter.api.Test;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.TypedAddress;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;


public class LockerPriceServiceTest {

    private final LockerPriceService lockerPriceService = new LockerPriceService();

    @Test
    void tenForSizeBelow3000AndWeightBelow2() {
        //given
        Package aPackage = Package.of(SMALLEST_SIZE_LOWEST_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(10L));
    }

    @Test
    void twentyForSizeBelow3000AndWeightBetween2_3() {
        //given
        Package aPackage = Package.of(SMALLEST_SIZE_LOW_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(20L));
    }

    @Test
    void twentyFiveForSizeBelow3000AndWeightBetween3_5() {
        //given
        Package aPackage = Package.of(SMALLEST_SIZE_MEDIUM_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(25L));
    }

    @Test
    void fortyForSizeBelow3000AndWeightAbove5() {
        //given
        Package aPackage = Package.of(SMALLEST_SIZE_BIG_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(40L));
    }

    @Test
    void thirtyForSizeBetween3000_6000AndWeightBelow2() {
        //given
        Package aPackage = Package.of(SMALL_SIZE_LOWEST_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(30L));
    }

    @Test
    void fortyForSizeBetween3000_6000AndWeightBetween2_3() {
        //given
        Package aPackage = Package.of(SMALL_SIZE_LOW_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(40L));
    }

    @Test
    void fortyFiveForSizeBetween3000_6000AndWeightBetween3_5() {
        //given
        Package aPackage = Package.of(SMALL_SIZE_MEDIUM_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(45L));
    }

    @Test
    void sixtyForSizeBetween3000_6000AndWeightAbove5() {
        //given
        Package aPackage = Package.of(SMALL_SIZE_BIG_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(60L));
    }

    @Test
    void fortyForSizeBetween6000_9000AndWeightBelow2() {
        //given
        Package aPackage = Package.of(MEDIUM_SIZE_LOWEST_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(40L));
    }

    @Test
    void fiftyForSizeBetween6000_9000AndWeightBetween2_3() {
        //given
        Package aPackage = Package.of(MEDIUM_SIZE_LOW_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(50L));
    }

    @Test
    void fiftyFiveForSizeBetween6000_9000AndWeightBetween3_5() {
        //given
        Package aPackage = Package.of(MEDIUM_SIZE_MEDIUM_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(55L));
    }

    @Test
    void seventyForSizeBetween6000_9000AndWeightAbove5() {
        //given
        Package aPackage = Package.of(MEDIUM_SIZE_BIG_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(70L));
    }

    @Test
    void sixtyFiveForSizeAbove9000AndWeightBelow2() {
        //given
        Package aPackage = Package.of(BIG_SIZE_LOWEST_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(65L));
    }

    @Test
    void seventyFiveForSizeAbove9000AndWeightBetween2_3() {
        //given
        Package aPackage = Package.of(BIG_SIZE_LOW_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(75L));
    }

    @Test
    void eightyForSizeAbove9000AndWeightBetween3_5() {
        //given
        Package aPackage = Package.of(BIG_SIZE_MEDIUM_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(80L));
    }

    @Test
    void ninetyFiveForSizeAbove9000AndWeightAbove5() {
        //given
        Package aPackage = Package.of(BIG_SIZE_BIG_WEIGHT, SIMPLE_USER_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_ADDRESS));

        //when
        BigDecimal price = lockerPriceService.calculatePriceForPackage(aPackage);

        //then
        assertThat(price).isEqualTo(BigDecimal.valueOf(95L));
    }
}