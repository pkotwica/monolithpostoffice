package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.map.service.MapService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.MAIN_PACKAGE;

@RunWith(MockitoJUnitRunner.class)
public class LockerTimeServiceTest {

    @Mock
    private MapService mapService;

    @InjectMocks
    private LockerTimeService lockerTimeService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void oneDayDeliveryForDistanceLessThan100KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(1L);
    }

    @Test
    public void threeDaysDeliveryForDistanceBetween100KM200KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(150);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(3L);
    }

    @Test
    public void fourDaysDeliveryForDistanceBetween200KM400KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(300);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(4L);
    }

    @Test
    public void fiveDayDeliveryForDistanceMoreThan400KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(450);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = lockerTimeService.getDeliveryTimePrediction(MAIN_PACKAGE);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(5L);
    }
}