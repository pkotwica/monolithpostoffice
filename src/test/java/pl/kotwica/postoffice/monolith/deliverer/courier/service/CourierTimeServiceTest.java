package pl.kotwica.postoffice.monolith.deliverer.courier.service;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.map.service.MapService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.MAIN_PACKAGE;

@RunWith(MockitoJUnitRunner.class)
public class CourierTimeServiceTest {

    private final static Integer LOWEST_PERCENTAGE_CAPACITY = 10;
    private final static Integer LOW_PERCENTAGE_CAPACITY = 40;
    private final static Integer HIGH_PERCENTAGE_CAPACITY = 70;
    private final static Integer HIGHEST_PERCENTAGE_CAPACITY = 95;

    @Mock
    private MapService mapService;

    @InjectMocks
    private CourierTimeService courierTimeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void oneDayDeliveryForDistanceLessThan100KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOWEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(1L);
    }

    @Test
    public void twoDaysDeliveryForDistanceBetween100KM200KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(150);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOWEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(2L);
    }

    @Test
    public void threeDaysDeliveryForDistanceBetween200KM400KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(300);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOWEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(3L);
    }

    @Test
    public void fourDayDeliveryForDistanceMoreThan400KM() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(450);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOWEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(4L);
    }

    @Test
    public void oneDayDeliveryForBusyPercentageCapacityBelow10() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOWEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(1L);
    }

    @Test
    public void twoDayDeliveryForBusyPercentageCapacityBetween30_60() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, LOW_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(2L);
    }

    @Test
    public void threeDayDeliveryForBusyPercentageCapacityBetween60_90() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, HIGH_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(3L);
    }

    @Test
    public void fourDayDeliveryForBusyPercentageCapacityAbove90() {
        //given
        when(mapService.getDistance(any(String.class), any(String.class))).thenReturn(50);
        final LocalDateTime now = LocalDateTime.now();

        //when
        LocalDateTime timePrediction = courierTimeService.getDeliveryTimePrediction(MAIN_PACKAGE, HIGHEST_PERCENTAGE_CAPACITY);

        //then
        assertThat(now.until(timePrediction, ChronoUnit.DAYS)).isEqualTo(4L);
    }
}