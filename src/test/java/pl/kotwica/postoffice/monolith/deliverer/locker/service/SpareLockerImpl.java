package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import lombok.AllArgsConstructor;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.Locker;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.SpareLocker;

@AllArgsConstructor
public class SpareLockerImpl implements SpareLocker {

    private final Locker locker;

    @Override
    public Locker getLocker() {
        return locker;
    }
}
