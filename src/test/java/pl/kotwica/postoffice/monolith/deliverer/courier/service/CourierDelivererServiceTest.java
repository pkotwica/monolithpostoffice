package pl.kotwica.postoffice.monolith.deliverer.courier.service;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.Courier;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackageStatus;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierRepository;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.packages.model.PackageFeedback;
import pl.kotwica.postoffice.monolith.packages.model.PackageInfo;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.SIMPLE_ID_ONE;

@RunWith(MockitoJUnitRunner.class)
public class CourierDelivererServiceTest {
    @Mock
    private CourierRepository courierRepository;
    @Mock
    private CourierPackageRepository courierPackageRepository;
    @Mock
    private CourierPriceService courierPriceService;
    @Mock
    private CourierTimeService courierTimeService;

    @InjectMocks
    private CourierDelivererService courierDelivererService;

    private final static Courier COURIER = new Courier(SIMPLE_ID_ONE, "Name", "Last", 10);

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void infoDataIsNoEmpty() {
        //when
        DelivererInfoData infoData = courierDelivererService.getInfoData();

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(infoData).isNotNull();
        softly.assertThat(infoData.isActive()).isNotNull();
        softly.assertThat(infoData.getDelivererID()).isNotNull();
        softly.assertThat(infoData.getDelivererName()).isNotNull();
        softly.assertThat(infoData.getDelivererName()).isNotEmpty();
        softly.assertAll();
    }

    @Test
    public void getExactlyPackagesInfoOfOwner() {
        //given
        when(courierPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(COURIER_PACKAGE_INTRODUCED));

        //when
        List<PackageInfo> usersPackages = courierDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        assertThat(usersPackages.stream().map(PackageInfo::getId)).containsExactly(COURIER_PACKAGE_INTRODUCED.getId());
    }

    @Test
    public void packageInfoInListOfPackagesIsNotEmpty() {
        //given
        when(courierPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(COURIER_PACKAGE_INTRODUCED));

        //when
        List<PackageInfo> usersPackages = courierDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(usersPackages.get(0).getId()).isNotNull();
        softly.assertThat(usersPackages.get(0).getSourceAddress()).isNotNull();
        softly.assertThat(usersPackages.get(0).getDestinationAddress()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPackageSize()).isNotNull();
        softly.assertThat(usersPackages.get(0).getSendingDate()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPredictionDeliveryDate()).isNotNull();
        softly.assertThat(usersPackages.get(0).getPrice()).isNotNull();
        softly.assertThat(usersPackages.get(0).getAdditionalDelivererInfo()).isNotNull();
        softly.assertAll();
    }

    @Test
    public void emptyListOfPackagesInfoIfOwnerHasNoPackages() {
        //given
        when(courierPackageRepository.findAllByMainPackage_Owner(SIMPLE_USER_ONE)).thenReturn(Collections.emptyList());

        //when
        List<PackageInfo> usersPackages = courierDelivererService.getUsersPackages(SIMPLE_USER_ONE);

        //then
        assertThat(usersPackages).isEmpty();
    }

    @Test
    public void packageInfoForSpecifiedPackageIdIsNotEmpty() {
        //given
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER_PACKAGE_INTRODUCED));

        //when
        PackageFeedback packageFeedback = courierDelivererService.getPackageInfo(SIMPLE_ID_ONE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(packageFeedback.isSuccess()).isTrue();
        softly.assertThat(packageFeedback.getPackageInfo()).isNotNull();
        softly.assertAll();
    }

    @Test
    public void errorPackageInfoIfSpecifiedIdIsWrong() {
        //given
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.empty());

        //when
        PackageFeedback packageFeedback = courierDelivererService.getPackageInfo(SIMPLE_ID_ONE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void errorIfSendPackageWithNoSufficientCourier() {
        //given
        when(courierRepository.findAllByCapacityGreaterThan(0)).thenReturn(Collections.emptyList());

        //when
        PackageFeedback packageFeedback = courierDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void errorIfSendPackageWithNoBusyCourier() {
        //given
        when(courierRepository.findAllByCapacityGreaterThan(0)).thenReturn(Collections.singletonList(new SpareCourierImpl(SIMPLE_ID_ONE, 10)));
        when(courierPackageRepository.countAllByCourier_IdAndStatusIn(SIMPLE_ID_ONE, Arrays.asList(CourierPackageStatus.CONFIRMED.name(), CourierPackageStatus.ON_THE_WAY.name()))).thenReturn(10);

        //when
        PackageFeedback packageFeedback = courierDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void successSendPackageFeedbackIsNotEmpty() {
        //given
        when(courierRepository.findAllByCapacityGreaterThan(0)).thenReturn(Collections.singletonList(new SpareCourierImpl(SIMPLE_ID_ONE, 10)));
        when(courierRepository.findById(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER));
        when(courierPackageRepository.countAllByCourier_IdAndStatusIn(SIMPLE_ID_ONE, Arrays.asList(CourierPackageStatus.CONFIRMED.name(), CourierPackageStatus.ON_THE_WAY.name()))).thenReturn(0);
        when(courierPackageRepository.save(any(CourierPackage.class))).thenReturn(COURIER_PACKAGE_INTRODUCED);
        when(courierPriceService.calculatePriceForPackage(MAIN_PACKAGE)).thenReturn(BigDecimal.TEN);
        when(courierTimeService.getDeliveryTimePrediction(eq(MAIN_PACKAGE), any(Integer.class))).thenReturn(SIMPLE_DATE);

        //when
        PackageFeedback packageFeedback = courierDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(packageFeedback.isSuccess()).isTrue();
        softly.assertThat(packageFeedback.getPackageInfo()).isNotNull();
        softly.assertThat(packageFeedback.getPackageInfo().getPrice()).isEqualByComparingTo(BigDecimal.TEN);
        softly.assertThat(packageFeedback.getPackageInfo().getPredictionDeliveryDate()).isEqualTo(SIMPLE_DATE);
        softly.assertAll();
    }

    @Test
    public void successSendPackageSaveNewPackage() {
        //given
        when(courierRepository.findAllByCapacityGreaterThan(0)).thenReturn(Collections.singletonList(new SpareCourierImpl(SIMPLE_ID_ONE, 10)));
        when(courierRepository.findById(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER));
        when(courierPackageRepository.countAllByCourier_IdAndStatusIn(SIMPLE_ID_ONE, Arrays.asList(CourierPackageStatus.CONFIRMED.name(), CourierPackageStatus.ON_THE_WAY.name()))).thenReturn(0);
        when(courierPackageRepository.save(any(CourierPackage.class))).thenReturn(COURIER_PACKAGE_INTRODUCED);
        when(courierPriceService.calculatePriceForPackage(MAIN_PACKAGE)).thenReturn(BigDecimal.TEN);
        when(courierTimeService.getDeliveryTimePrediction(eq(MAIN_PACKAGE), any(Integer.class))).thenReturn(SIMPLE_DATE);

        //when
        courierDelivererService.sendPackage(MAIN_PACKAGE);

        //then
        verify(courierPackageRepository, times(1)).save(any(CourierPackage.class));
    }

    @Test
    public void confirmedPackageIsSaved() {
        //given
        COURIER_PACKAGE_INTRODUCED.setStatus(CourierPackageStatus.INTRODUCED.name());
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER_PACKAGE_INTRODUCED));

        //when
        boolean resultConfirmation = courierDelivererService.confirmPackage(CONFIRM_SHIP_FORM);

        //then
        verify(courierPackageRepository, times(1)).save(any(CourierPackage.class));
        assertThat(resultConfirmation).isTrue();
        assertThat(COURIER_PACKAGE_INTRODUCED.getStatus()).isEqualTo(CourierPackageStatus.CONFIRMED.name());
    }

    @Test
    public void declinePackageIsSaved() {
        //given
        COURIER_PACKAGE_INTRODUCED.setStatus(CourierPackageStatus.INTRODUCED.name());
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER_PACKAGE_INTRODUCED));

        //when
        boolean resultConfirmation = courierDelivererService.confirmPackage(DECLINE_SHIP_FORM);

        //then
        verify(courierPackageRepository, times(1)).save(any(CourierPackage.class));
        assertThat(resultConfirmation).isTrue();
        assertThat(COURIER_PACKAGE_INTRODUCED.getStatus()).isEqualTo(CourierPackageStatus.DECLINED.name());
    }

    @Test
    public void errorIfPackageDuringConfirmationHasOtherStatusThatIntroduced() {
        //given
        COURIER_PACKAGE_CONFIRMED.setStatus(CourierPackageStatus.CONFIRMED.name());
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.of(COURIER_PACKAGE_CONFIRMED));

        //when
        boolean resultConfirmation = courierDelivererService.confirmPackage(DECLINE_SHIP_FORM);

        //then
        verify(courierPackageRepository, times(0)).save(any(CourierPackage.class));
        assertThat(resultConfirmation).isFalse();
    }

    @Test
    public void errorIfPackageNotExists() {
        //given
        when(courierPackageRepository.findByMainPackage_Id(SIMPLE_ID_ONE)).thenReturn(Optional.empty());

        //when
        boolean resultConfirmation = courierDelivererService.confirmPackage(DECLINE_SHIP_FORM);

        //then
        verify(courierPackageRepository, times(0)).save(any(CourierPackage.class));
        assertThat(resultConfirmation).isFalse();
    }
}