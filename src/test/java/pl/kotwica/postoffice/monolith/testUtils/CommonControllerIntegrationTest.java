package pl.kotwica.postoffice.monolith.testUtils;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData.LOGGED_USER_EMAIL;
import static pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData.LOGGED_USER_PASS;

@WebIntegrationTest
public class CommonControllerIntegrationTest {
    private static final String PROTECTED_URL_PREFIX = "/api";
    private static final String AUTH_HEADER_KEY = "Authorization";
    private static final String AUTH_HEADER_VALUE = "Bearer ";
    private static final String LOG_IN_FORM = "{\n" +
            "    \"username\": \"" + LOGGED_USER_EMAIL + "\",\n" +
            "    \"password\": \"" + LOGGED_USER_PASS + "\"\n" +
            "}";
    private static final String TOKEN_URL_PATH = "/token";
    private static final String TOKEN_JSON_FIELD = "tokenCode";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    protected IntegrationTestData integrationTestData;

    protected MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    protected String logStandardUser() throws Exception {
        String contentAsString = mockMvc.perform(post(TOKEN_URL_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(LOG_IN_FORM)).andReturn().getResponse().getContentAsString();

        return JsonPath.parse(contentAsString).read(TOKEN_JSON_FIELD);
    }

    protected ResultActions performProtectedPostRequest(String url, String authToken, String content) throws Exception {
        return performProtectedRequest(post(PROTECTED_URL_PREFIX + url), authToken, content);
    }

    protected ResultActions performProtectedGetRequest(String url, String authToken, String content) throws Exception {
        return performProtectedRequest(get(PROTECTED_URL_PREFIX + url), authToken, content);
    }

    protected ResultActions performProtectedDeleteRequest(String url, String authToken, String content) throws Exception {
        return performProtectedRequest(delete(PROTECTED_URL_PREFIX + url), authToken, content);
    }

    protected ResultActions performProtectedRequest(MockHttpServletRequestBuilder request, String authToken, String content) throws Exception {
        return mockMvc.perform(request
                .header(AUTH_HEADER_KEY, AUTH_HEADER_VALUE + authToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
        );
    }
}
