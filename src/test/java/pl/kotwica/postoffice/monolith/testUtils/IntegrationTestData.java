package pl.kotwica.postoffice.monolith.testUtils;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.addressBook.repo.AddressBookRepository;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.Courier;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackageStatus;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierRepository;
import pl.kotwica.postoffice.monolith.map.model.City;
import pl.kotwica.postoffice.monolith.map.model.Country;
import pl.kotwica.postoffice.monolith.map.model.Postcode;
import pl.kotwica.postoffice.monolith.map.model.Road;
import pl.kotwica.postoffice.monolith.map.repo.CityRepository;
import pl.kotwica.postoffice.monolith.map.repo.CountryRepository;
import pl.kotwica.postoffice.monolith.map.repo.PostcodeRepository;
import pl.kotwica.postoffice.monolith.map.repo.RoadRepository;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;
import pl.kotwica.postoffice.monolith.packages.model.TypedAddress;
import pl.kotwica.postoffice.monolith.packages.repo.PackageRepository;
import pl.kotwica.postoffice.monolith.packages.repo.TypedAddressRepository;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static pl.kotwica.postoffice.monolith.testUtils.TestData.SHIPMENT_FORM_ADDRESS;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.SHIPMENT_FORM_DESTINATION_ADDRESS;

@Component
@Profile("test")
public class IntegrationTestData {
    public static final String LOGGED_USER_EMAIL = "logged@email.com";
    public static final String NOT_EXISTING_USER_EMAIL = "email@not_existing_email.com";
    public static final String LOGGED_USER_PASS = "pass";

    public static final ShipmentFormAddress ADDRESS_BOOK_ADDRESS_FORM = new ShipmentFormAddress("John", "Doe", "123123123", "email@email1.com", "Country", "City", "10-100", "Road", 1);
    private static final Courier SAMPLE_COURIER = Courier.of("Jan", "Blachowicz", 10);

    private static final TypedAddress SOURCE_TYPED_ADDRESS = TypedAddress.of(SHIPMENT_FORM_ADDRESS);
    private static final TypedAddress DESTINATION_TYPED_ADDRESS = TypedAddress.of(SHIPMENT_FORM_DESTINATION_ADDRESS);

    private final PasswordService passwordEncoder;
    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private final PostcodeRepository postcodeRepository;
    private final RoadRepository roadRepository;
    private final AddressBookRepository addressBookRepository;
    private final CourierRepository courierRepository;
    private final TypedAddressRepository typedAddressRepository;
    private final PackageRepository packageRepository;
    private final CourierPackageRepository courierPackageRepository;

    public IntegrationTestData(PasswordService passwordEncoder, UserRepository userRepository, CountryRepository countryRepository, CityRepository cityRepository, PostcodeRepository postcodeRepository, RoadRepository roadRepository, AddressBookRepository addressBookRepository, CourierRepository courierRepository, TypedAddressRepository typedAddressRepository, PackageRepository packageRepository, CourierPackageRepository courierPackageRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
        this.postcodeRepository = postcodeRepository;
        this.roadRepository = roadRepository;
        this.addressBookRepository = addressBookRepository;
        this.courierRepository = courierRepository;
        this.typedAddressRepository = typedAddressRepository;
        this.packageRepository = packageRepository;
        this.courierPackageRepository = courierPackageRepository;
    }

    public User addStandardUser() {
        return userRepository.save(User.of(new RegisterForm("first", "last", LOGGED_USER_EMAIL, LOGGED_USER_PASS, LOGGED_USER_PASS), passwordEncoder.encode(LOGGED_USER_PASS)));
    }

    public void addMapAddresses() {
        Country country = countryRepository.save(new Country(null, "Polska"));
        City city = cityRepository.save(new City(null, "Wroclaw", country));

        Postcode postcode = postcodeRepository.save(new Postcode(null, "WROC-1", city));
        roadRepository.save(new Road(null, "Kamienna", 1, 10, postcode));

        postcode = postcodeRepository.save(new Postcode(null, "WROC-2", city));
        roadRepository.save(new Road(null, "Komandorska", 1, 50, postcode));
    }

    public Address addAddressBookExampleAddress(User user) {
        return addressBookRepository.save(Address.of(ADDRESS_BOOK_ADDRESS_FORM, user));
    }

    public Package addCourierPackageWithStatus(User user, Courier courier, CourierPackageStatus status) {
        TypedAddress source = typedAddressRepository.save(SOURCE_TYPED_ADDRESS);
        TypedAddress destination = typedAddressRepository.save(DESTINATION_TYPED_ADDRESS);

        Package newPackage = packageRepository.save(new Package(null, source, destination, user, 10, 10, 10, 10, LocalDateTime.now(), 10));
        CourierPackage courierPackage = courierPackageRepository.save(new CourierPackage(null, status.name(), BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), newPackage, courier));

        return courierPackage.getMainPackage();
    }

    public Courier addCourier() {
        return courierRepository.save(SAMPLE_COURIER);
    }

    public Courier setCourierCapacity(Integer capacity, Courier courier) {
        courier.setCapacity(capacity);
        return courierRepository.save(courier);
    }
}
