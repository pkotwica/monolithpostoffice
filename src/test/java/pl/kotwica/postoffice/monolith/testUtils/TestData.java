package pl.kotwica.postoffice.monolith.testUtils;

import pl.kotwica.postoffice.monolith.deliverer.courier.model.AdditionalCourierDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackageStatus;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.AdditionalLockerDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackage;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackageStatus;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

public class TestData {
    public static final Integer SIMPLE_ID_ONE = 1;
    public static final Integer SIMPLE_ID_TWO = 2;

    public static final String SIMPLE_FIRST_NAME = "Jan";
    public static final String SIMPLE_LAST_NAME = "Kowalski";
    public static final String SIMPLE_EMAIL = "example@mail.com";
    public static final String PHONE_NUMBER = "123098123";
    public static final String SIMPLE_PASS = "simple_password";

    public static final String ADDRESS_COUNTRY = "Polska";
    public static final String ADDRESS_CITY = "Wroclaw";
    public static final String ADDRESS_CITY_DESTINATION = "Karkow";
    public static final String ADDRESS_POSTCODE = "WROC-1";
    public static final String ADDRESS_POSTCODE_DESTINATION = "WROC-2";
    public static final String ADDRESS_ROAD = "Legnicka";
    public static final String ADDRESS_ROAD_DESTINATION = "Komandorska";
    public static final Integer ADDRESS_NUMBER = 10;

    public static final ShipmentFormAddress SHIPMENT_FORM_ADDRESS = new ShipmentFormAddress(SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, PHONE_NUMBER, SIMPLE_EMAIL, ADDRESS_COUNTRY, ADDRESS_CITY, ADDRESS_POSTCODE, ADDRESS_ROAD, ADDRESS_NUMBER);
    public static final ShipmentFormAddress SHIPMENT_FORM_DESTINATION_ADDRESS = new ShipmentFormAddress(SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, PHONE_NUMBER, SIMPLE_EMAIL, ADDRESS_COUNTRY, ADDRESS_CITY_DESTINATION, ADDRESS_POSTCODE_DESTINATION, ADDRESS_ROAD_DESTINATION, ADDRESS_NUMBER);

    public static final User SIMPLE_USER_ONE = new User(SIMPLE_ID_ONE, SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, SIMPLE_PASS, SIMPLE_EMAIL, null, null);
    public static final User SIMPLE_USER_TWO = new User(SIMPLE_ID_TWO, SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, SIMPLE_PASS, SIMPLE_EMAIL, null, null);

    public static final LocalDateTime SIMPLE_DATE = LocalDateTime.now();

    public static final Integer DELIVERER_COURIER_ID = 10;
    public static final Integer DELIVERER_LOCKER_ID = 20;
    public static final String DELIVERER_LOCKER_NAME = "Locker";
    public static final String DELIVERER_COURIER_NAME = "Courier";

    public static final DelivererInfoData LOCKER_ACTIVE_DELIVERER_INFO_DATA = new DelivererInfoData(DELIVERER_LOCKER_ID, DELIVERER_LOCKER_NAME, true);
    public static final DelivererInfoData LOCKER_NON_ACTIVE_DELIVERER_INFO_DATA = new DelivererInfoData(DELIVERER_LOCKER_ID, DELIVERER_LOCKER_NAME, false);
    public static final DelivererInfoData COURIER_ACTIVE_DELIVERER_INFO_DATA = new DelivererInfoData(DELIVERER_COURIER_ID, DELIVERER_COURIER_NAME, true);
    public static final DelivererInfoData COURIER_NON_ACTIVE_DELIVERER_INFO_DATA = new DelivererInfoData(DELIVERER_COURIER_ID, DELIVERER_COURIER_NAME, false);

    public final static Package MAIN_PACKAGE = new Package(SIMPLE_ID_ONE, TypedAddress.of(SHIPMENT_FORM_ADDRESS), TypedAddress.of(SHIPMENT_FORM_DESTINATION_ADDRESS), SIMPLE_USER_ONE, 10, 10, 10, 10, SIMPLE_DATE, 10);

    public final static LockerPackage LOCKER_PACKAGE_INTRODUCED = new LockerPackage(SIMPLE_ID_ONE, LockerPackageStatus.INTRODUCED.name(), BigDecimal.TEN, SIMPLE_DATE, MAIN_PACKAGE, null, null);
    public final static LockerPackage LOCKER_PACKAGE_CONFIRMED = new LockerPackage(SIMPLE_ID_ONE, LockerPackageStatus.CONFIRMED.name(), BigDecimal.TEN, SIMPLE_DATE, MAIN_PACKAGE, null, null);
    public final static CourierPackage COURIER_PACKAGE_INTRODUCED = new CourierPackage(SIMPLE_ID_ONE, CourierPackageStatus.INTRODUCED.name(), BigDecimal.TEN, SIMPLE_DATE, MAIN_PACKAGE, null);
    public final static CourierPackage COURIER_PACKAGE_CONFIRMED = new CourierPackage(SIMPLE_ID_ONE, CourierPackageStatus.CONFIRMED.name(), BigDecimal.TEN, SIMPLE_DATE, MAIN_PACKAGE, null);
    public final static AdditionalLockerDelivererInfo ADDITIONAL_LOCKER_DELIVERER_INFO = AdditionalLockerDelivererInfo.of(LOCKER_ACTIVE_DELIVERER_INFO_DATA, LockerPackageStatus.INTRODUCED, Collections.emptyList(), Collections.emptyList());
    public final static AdditionalCourierDelivererInfo ADDITIONAL_COURIER_DELIVERER_INFO = AdditionalCourierDelivererInfo.of(COURIER_ACTIVE_DELIVERER_INFO_DATA, CourierPackageStatus.INTRODUCED);
    public final static PackageInfo LOCKER_PACKAGE_INFO = PackageInfo.of(LOCKER_PACKAGE_INTRODUCED, ADDITIONAL_LOCKER_DELIVERER_INFO);
    public final static PackageInfo COURIER_PACKAGE_INFO = PackageInfo.of(COURIER_PACKAGE_INTRODUCED, ADDITIONAL_COURIER_DELIVERER_INFO);

    public final static PackageFeedback SUCCESS_PACKAGE_FEEDBACK = new PackageFeedback(COURIER_PACKAGE_INFO);

    public final static ShipmentForm SHIPMENT_FORM_FOR_COURIER = new ShipmentForm(SHIPMENT_FORM_ADDRESS, SHIPMENT_FORM_DESTINATION_ADDRESS, new PackageSize(10, 10, 10, 10), DELIVERER_COURIER_ID);

    public final static String LOCKER_CODE = "WROC1";
    public final static String LOCKER_CODE_DESTINATION = "WROC2";

    public final static ConfirmShipForm CONFIRM_SHIP_FORM = new ConfirmShipForm(SIMPLE_ID_ONE, true, LOCKER_CODE, LOCKER_CODE_DESTINATION);
    public final static ConfirmShipForm DECLINE_SHIP_FORM = new ConfirmShipForm(SIMPLE_ID_ONE, false, LOCKER_CODE, LOCKER_CODE_DESTINATION);

    public static final ShipmentForm SMALLEST_SIZE_LOWEST_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(1, 1, 1, 1));
    public static final ShipmentForm SMALLEST_SIZE_LOW_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(1, 1, 1, 3));
    public static final ShipmentForm SMALLEST_SIZE_MEDIUM_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(1, 1, 1, 4));
    public static final ShipmentForm SMALLEST_SIZE_BIG_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(1, 1, 1, 6));
    public static final ShipmentForm SMALL_SIZE_LOWEST_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(15, 15, 15, 1));
    public static final ShipmentForm SMALL_SIZE_LOW_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(15, 15, 15, 3));
    public static final ShipmentForm SMALL_SIZE_MEDIUM_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(15, 15, 15, 4));
    public static final ShipmentForm SMALL_SIZE_BIG_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(15, 15, 15, 6));
    public static final ShipmentForm MEDIUM_SIZE_LOWEST_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(20, 20, 20, 1));
    public static final ShipmentForm MEDIUM_SIZE_LOW_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(20, 20, 20, 3));
    public static final ShipmentForm MEDIUM_SIZE_MEDIUM_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(20, 20, 20, 4));
    public static final ShipmentForm MEDIUM_SIZE_BIG_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(20, 20, 20, 6));
    public static final ShipmentForm BIG_SIZE_LOWEST_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(25, 25, 25, 1));
    public static final ShipmentForm BIG_SIZE_LOW_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(25, 25, 25, 3));
    public static final ShipmentForm BIG_SIZE_MEDIUM_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(25, 25, 25, 4));
    public static final ShipmentForm BIG_SIZE_BIG_WEIGHT = new ShipmentForm(DELIVERER_LOCKER_ID, new PackageSize(25, 25, 25, 6));
}
