package pl.kotwica.postoffice.monolith.packages.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.deliverer.courier.service.CourierDelivererService;
import pl.kotwica.postoffice.monolith.deliverer.locker.service.LockerDelivererService;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.deliverer.service.DelivererService;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.repo.PackageRepository;
import pl.kotwica.postoffice.monolith.packages.repo.TypedAddressRepository;
import pl.kotwica.postoffice.monolith.user.service.UtilUserService;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class PackageServiceTest {
    @Mock
    private PackageRepository packageRepository;

    @Mock
    private TypedAddressRepository typedAddressRepository;

    @Mock
    private UtilUserService utilUserService;


    @InjectMocks
    private PackageService packageService;

    @Spy
    private ArrayList<DelivererService> delivererServices;

    @Mock
    private CourierDelivererService courierDelivererService;

    @Mock
    private LockerDelivererService lockerDelivererService;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() {
        delivererServices.add(lockerDelivererService);
        delivererServices.add(courierDelivererService);
    }

    @Test
    public void onlyActiveDeliverersInfoData() {
        //given
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_NON_ACTIVE_DELIVERER_INFO_DATA);

        //when
        List<DelivererInfoData> activeDeliverersInfo = packageService.getActiveDeliverersInfo();

        //then
        assertThat(activeDeliverersInfo.stream().map(DelivererInfoData::getDelivererName).collect(Collectors.toList())).containsExactly(DELIVERER_COURIER_NAME);
    }

    @Test
    public void allDeliverersPackagesPresentForLoggedUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);
        when(courierDelivererService.getUsersPackages(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(COURIER_PACKAGE_INFO));
        when(lockerDelivererService.getUsersPackages(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(LOCKER_PACKAGE_INFO));

        //when
        List<PackageInfo> loggedUserPackages = packageService.getLoggedUserPackages();

        //then
        assertThat(loggedUserPackages).containsExactlyInAnyOrderElementsOf(Arrays.asList(LOCKER_PACKAGE_INFO, COURIER_PACKAGE_INFO));
    }

    @Test
    public void onlyActiveDeliverersPackagesPresentForLoggedUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(courierDelivererService.getUsersPackages(SIMPLE_USER_ONE)).thenReturn(Collections.singletonList(COURIER_PACKAGE_INFO));

        //when
        List<PackageInfo> loggedUserPackages = packageService.getLoggedUserPackages();

        //then
        assertThat(loggedUserPackages).containsExactlyInAnyOrderElementsOf(Collections.singletonList(COURIER_PACKAGE_INFO));
    }

    @Test
    public void emptyListIfNoPackagesForLoggedUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);
        when(courierDelivererService.getUsersPackages(SIMPLE_USER_ONE)).thenReturn(Collections.emptyList());
        when(lockerDelivererService.getUsersPackages(SIMPLE_USER_ONE)).thenReturn(Collections.emptyList());

        //when
        List<PackageInfo> loggedUserPackages = packageService.getLoggedUserPackages();

        //then
        assertThat(loggedUserPackages).isEmpty();
    }

    @Test
    public void emptyListOfPackagesIfNotLoggedUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());

        //when
        List<PackageInfo> loggedUserPackages = packageService.getLoggedUserPackages();

        //then
        assertThat(loggedUserPackages).isEmpty();
    }

    @Test
    public void bothTypedAddressIsSaved() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.emptyList());
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        verify(typedAddressRepository, times(2)).save(any(TypedAddress.class));
    }

    @Test
    public void typedAddressesNotSavedIfExists() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(TypedAddress.of(SHIPMENT_FORM_ADDRESS)));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        verify(typedAddressRepository, times(0)).save(any(TypedAddress.class));
    }

    @Test
    public void mainPackageSavedIfUserLogged() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(TypedAddress.of(SHIPMENT_FORM_ADDRESS)));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        verify(packageRepository, times(1)).save(any(Package.class));
    }

    @Test
    public void packageSendByChosenActiveDeliverer() {
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(TypedAddress.of(SHIPMENT_FORM_ADDRESS)));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);
        when(packageRepository.save(any(Package.class))).thenReturn(MAIN_PACKAGE);

        //when
        packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        verify(courierDelivererService, times(1)).sendPackage(MAIN_PACKAGE);
    }

    @Test
    public void errorFeedbackIfChosenDelivererIsNotActive() {
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(TypedAddress.of(SHIPMENT_FORM_ADDRESS)));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);
        when(packageRepository.save(any(Package.class))).thenReturn(MAIN_PACKAGE);

        //when
        PackageFeedback packageFeedback = packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
        verify(courierDelivererService, times(0)).sendPackage(any(Package.class));
    }

    @Test
    public void errorFeedbackIfNoActiveDeliverer() {
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(TypedAddress.of(SHIPMENT_FORM_ADDRESS)));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(packageRepository.save(any(Package.class))).thenReturn(MAIN_PACKAGE);

        //when
        PackageFeedback packageFeedback = packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
        verify(courierDelivererService, times(0)).sendPackage(any(Package.class));
    }

    @Test
    public void dataNotSavedIfUserNotLogged() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());

        //when
        packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        verify(packageRepository, times(0)).save(any(Package.class));
        verify(typedAddressRepository, times(0)).save(any(TypedAddress.class));
        verify(courierDelivererService, times(0)).sendPackage(any(Package.class));
        verify(lockerDelivererService, times(0)).sendPackage(any(Package.class));
    }

    @Test
    public void errorFeedbackIfUserNotLogged() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());

        //when
        PackageFeedback packageFeedback = packageService.placeShipment(SHIPMENT_FORM_FOR_COURIER);

        //then
        assertThat(packageFeedback.isSuccess()).isFalse();
    }

    @Test
    public void packageSuccessfullyConfirmed() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));
        when(courierDelivererService.confirmPackage(CONFIRM_SHIP_FORM)).thenReturn(true);
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        boolean confirmationResult = packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        assertThat(confirmationResult).isTrue();
    }

    @Test
    public void packageNotConfirmedIfWrongPackageId() {
        //given
        when(packageRepository.findById(SIMPLE_ID_ONE)).thenReturn(Optional.empty());

        //when
        boolean confirmationResult = packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        assertThat(confirmationResult).isFalse();
    }

    @Test
    public void packageNotConfirmedIfNotBelongsToUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_TWO));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));

        //when
        boolean confirmationResult = packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        assertThat(confirmationResult).isFalse();
    }

    @Test
    public void packageNotConfirmedIfUserNotLogged() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.empty());
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));

        //when
        boolean confirmationResult = packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        assertThat(confirmationResult).isFalse();
    }

    @Test
    public void packageConfirmedForSpecifiedDeliverer() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));
        when(courierDelivererService.confirmPackage(CONFIRM_SHIP_FORM)).thenReturn(true);
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        verify(courierDelivererService, times(1)).confirmPackage(CONFIRM_SHIP_FORM);
        verify(lockerDelivererService, times(0)).confirmPackage(CONFIRM_SHIP_FORM);
    }

    @Test
    public void packageNotConfirmedIfSpecifiedDelivererIsNotActive() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        boolean confirmationResult = packageService.confirmShipment(CONFIRM_SHIP_FORM);

        //then
        verify(courierDelivererService, times(0)).confirmPackage(CONFIRM_SHIP_FORM);
        verify(lockerDelivererService, times(0)).confirmPackage(CONFIRM_SHIP_FORM);
        assertThat(confirmationResult).isFalse();
    }

    @Test
    public void packageInfoSuccessfullyReturnedForSpecifiedDeliverer() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);
        when(courierDelivererService.getPackageInfo(MAIN_PACKAGE.getId())).thenReturn(SUCCESS_PACKAGE_FEEDBACK);

        //when
        PackageFeedback packageInfo = packageService.getPackageInfo(MAIN_PACKAGE.getId());

        //then
        assertThat(packageInfo).isEqualTo(SUCCESS_PACKAGE_FEEDBACK);
    }

    @Test
    public void packageInfoErrorIfWrongPackageId() {
        //given
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        //when
        PackageFeedback packageInfo = packageService.getPackageInfo(MAIN_PACKAGE.getId());

        //then
        assertThat(packageInfo.isSuccess()).isEqualTo(false);
    }

    @Test
    public void packageInfoErrorIfSpecifiedDelivererIsNotActive() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_ONE));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));
        when(courierDelivererService.getInfoData()).thenReturn(COURIER_NON_ACTIVE_DELIVERER_INFO_DATA);
        when(lockerDelivererService.getInfoData()).thenReturn(LOCKER_ACTIVE_DELIVERER_INFO_DATA);

        //when
        PackageFeedback packageInfo = packageService.getPackageInfo(MAIN_PACKAGE.getId());

        //then
        assertThat(packageInfo.isSuccess()).isFalse();
    }

    @Test
    public void packageInfoErrorIfPackageNotBelongsToLoggedUser() {
        //given
        when(utilUserService.getLoggedUser()).thenReturn(Optional.of(SIMPLE_USER_TWO));
        when(packageRepository.findById(any(Integer.class))).thenReturn(Optional.of(MAIN_PACKAGE));

        //when
        PackageFeedback packageInfo = packageService.getPackageInfo(MAIN_PACKAGE.getId());

        //then
        assertThat(packageInfo.isSuccess()).isFalse();
    }
}