package pl.kotwica.postoffice.monolith.packages.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.Courier;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackageStatus;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.testUtils.CommonControllerIntegrationTest;
import pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData;
import pl.kotwica.postoffice.monolith.user.model.User;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData.LOGGED_USER_EMAIL;

class PackageControllerIntegrationTest extends CommonControllerIntegrationTest {

    private static final String VALID_SHIPPING_FORM = "{\n" +
            "    \"sourceAddress\": {\n" +
            "        \"firstName\": \"Jan\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"WROC-1\",\n" +
            "        \"road\": \"Kamienna\",\n" +
            "        \"number\": 5\n" +
            "    },\n" +
            "    \"destinationAddress\": {\n" +
            "        \"firstName\": \"Jan\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"WROC-2\",\n" +
            "        \"road\": \"Komandorska\",\n" +
            "        \"number\": 2\n" +
            "    },\n" +
            "    \"packageSize\": {\n" +
            "        \"x\": 10,\n" +
            "        \"y\": 10,\n" +
            "        \"z\": 10,\n" +
            "        \"weight\": 30\n" +
            "    },\n" +
            "    \"delivererID\": 10\n" +
            "}";

    private static final String INCOMPLETE_SHIPPING_FORM = "{\n" +
            "    \"sourceAddress\": {\n" +
            "        \"firstName\": \"\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"WROC-1\",\n" +
            "        \"road\": \"Kamienna\",\n" +
            "        \"number\": 5\n" +
            "    },\n" +
            "    \"destinationAddress\": {\n" +
            "        \"firstName\": \"Jan\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"WROC-2\",\n" +
            "        \"road\": \"Komandorska\",\n" +
            "        \"number\": 2\n" +
            "    },\n" +
            "    \"packageSize\": {\n" +
            "        \"x\": 10,\n" +
            "        \"y\": 10,\n" +
            "        \"z\": 10,\n" +
            "        \"weight\": 30\n" +
            "    },\n" +
            "    \"delivererID\": 10\n" +
            "}";

    private static final String NOT_EXISTING_ADDRESS_SHIPPING_FORM = "{\n" +
            "    \"sourceAddress\": {\n" +
            "        \"firstName\": \"Jan\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"WROC-1\",\n" +
            "        \"road\": \"Kamienna\",\n" +
            "        \"number\": 5\n" +
            "    },\n" +
            "    \"destinationAddress\": {\n" +
            "        \"firstName\": \"Jan\",\n" +
            "        \"lastName\": \"Kowalski\",\n" +
            "        \"phoneNumber\": \"191919191\",\n" +
            "        \"email\": \"email@courierpackage.com\",\n" +
            "        \"country\": \"Polska\",\n" +
            "        \"city\": \"Wroclaw\",\n" +
            "        \"postcode\": \"NOT_EXISTING_POSTCODE\",\n" +
            "        \"road\": \"Komandorska\",\n" +
            "        \"number\": 2\n" +
            "    },\n" +
            "    \"packageSize\": {\n" +
            "        \"x\": 10,\n" +
            "        \"y\": 10,\n" +
            "        \"z\": 10,\n" +
            "        \"weight\": 30\n" +
            "    },\n" +
            "    \"delivererID\": 10\n" +
            "}";

    private final static String CONFIRM_SHIP_FORM_TEMPLATE = "{\n" +
            "    \"packageID\": %d,\n" +
            "    \"confirm\": true,\n" +
            "    \"sourceLockerCode\": \"\",\n" +
            "    \"destinationLockerCode\": \"\"\n" +
            "}";

    private static User standardUser;
    private static Courier courier;
    private static Package packageWithDeliveredStatus;
    private static Package packageWithIntroducedStatus;

    @BeforeAll
    public static void init(@Autowired IntegrationTestData integrationTestData) {
        standardUser = integrationTestData.addStandardUser();
        integrationTestData.addMapAddresses();
        courier = integrationTestData.addCourier();
        packageWithDeliveredStatus = integrationTestData.addCourierPackageWithStatus(standardUser, courier, CourierPackageStatus.DELIVERED);
        packageWithIntroducedStatus = integrationTestData.addCourierPackageWithStatus(standardUser, courier, CourierPackageStatus.INTRODUCED);
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void getPackagesCauseOkStatusWithListOfUsersPackages() throws Exception {
        performProtectedGetRequest("/packages", logStandardUser(), "")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id", hasItem(packageWithDeliveredStatus.getId())))
                .andExpect(jsonPath("$.[*].id", hasItem(packageWithIntroducedStatus.getId())));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void checkPackageStatusCauseOkStatusWithSpecifiedPackageStatus() throws Exception {
        performProtectedGetRequest("/packages/check-status?packageID=" + packageWithDeliveredStatus.getId(), logStandardUser(), "")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.packageInfo").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.destinationAddress").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.destinationAddress").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.packageSize").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.sendingDate").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.predictionDeliveryDate").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.price").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.additionalDelivererInfo").isNotEmpty())
                .andExpect(jsonPath("$.packageInfo.additionalDelivererInfo.courierPackageStatus").value(CourierPackageStatus.DELIVERED.name()));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void checkingPackageStatusWithWrongIDCauseConflictStatusWithNoSuccessJson() throws Exception {
        performProtectedGetRequest("/packages/check-status?packageID=" + (packageWithDeliveredStatus.getId() + 100), logStandardUser(), "")
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.success").value(false));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void getActiveDeliverersCauseOkStatusWithListOfActiveDeliverers() throws Exception {
        performProtectedGetRequest("/packages/deliverers", logStandardUser(), "")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void successfulShippingCauseOkStatusWithPackageFeedbackJson() throws Exception {
        performProtectedPostRequest("/packages/ship", logStandardUser(), VALID_SHIPPING_FORM)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value("Package successfully added - to confirm"))
                .andExpect(jsonPath("$.packageInfo").isNotEmpty());
    }

    @Test
    void incompleteShippingCauseBadRequestWithErrorJson() throws Exception {
        performProtectedPostRequest("/packages/ship", logStandardUser(), INCOMPLETE_SHIPPING_FORM)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Form validation error"))
                .andExpect(jsonPath("$.errors[*].value", hasItem("must not be empty")));
    }

    @Test
    void notExistingAddressCauseBadRequestWithErrorJson() throws Exception {
        performProtectedPostRequest("/packages/ship", logStandardUser(), NOT_EXISTING_ADDRESS_SHIPPING_FORM)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Form validation error"))
                .andExpect(jsonPath("$.errors[*].value", hasItem("Given address does not exist.")));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void otherErrorShippingCauseConflictStatusWithErrorJson() throws Exception {
        courier = integrationTestData.setCourierCapacity(0, courier);

        performProtectedPostRequest("/packages/ship", logStandardUser(), VALID_SHIPPING_FORM)
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.success").value(false));

        courier = integrationTestData.setCourierCapacity(10, courier);
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void successfulShippingConfirmationCauseOkStatusWithSuccessJson() throws Exception {
        performProtectedPostRequest("/packages/confirm-ship", logStandardUser(), String.format(CONFIRM_SHIP_FORM_TEMPLATE, packageWithIntroducedStatus.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value("Shipment confirmed successfully"));
    }

    @Test
    @WithMockUser(LOGGED_USER_EMAIL)
    void otherErrorShippingConfirmationCauseConflictStatusWithErrorJson() throws Exception {
        performProtectedPostRequest("/packages/confirm-ship", logStandardUser(), String.format(CONFIRM_SHIP_FORM_TEMPLATE, packageWithDeliveredStatus.getId()))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Wrong data"));
    }
}