package pl.kotwica.postoffice.monolith.user.service;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordService passwordEncoder;

    @InjectMocks
    private UserService userService;

    private static final RegisterForm REGISTER_FORM = new RegisterForm(SIMPLE_FIRST_NAME, SIMPLE_LAST_NAME, SIMPLE_EMAIL, SIMPLE_PASS, SIMPLE_PASS);
    private static final List<User> USER_LIST = Arrays.asList(SIMPLE_USER_ONE, SIMPLE_USER_TWO);

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void userAddedIfEmailNotExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.empty());

        //when
        boolean userAddMethodResult = userService.addNewUser(REGISTER_FORM);

        //then
        assertThat(userAddMethodResult).isTrue();
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void userNotAddedIfEmailExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));

        //when
        boolean userAddMethodResult = userService.addNewUser(REGISTER_FORM);

        //then
        assertThat(userAddMethodResult).isFalse();
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    public void presentUserIfEmailExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));

        //when
        Optional<User> userByEmailMethodResult = userService.getUserByEmail(SIMPLE_EMAIL);

        //then
        assertThat(userByEmailMethodResult.get()).isEqualTo(SIMPLE_USER_ONE);
    }

    @Test
    public void notPresentUserIfEmailNotExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.empty());

        //when
        Optional<User> userByEmailMethodResult = userService.getUserByEmail(SIMPLE_EMAIL);

        //then
        assertThat(userByEmailMethodResult.isPresent()).isFalse();
    }

    @Test
    public void exactSameListOfUsers() {
        //given
        when(userRepository.findAll()).thenReturn(USER_LIST);

        //when
        List<User> allUsersMethodResult = userService.getAllUsers();

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(allUsersMethodResult).isEqualTo(USER_LIST);
        softly.assertThat(allUsersMethodResult).containsExactly(SIMPLE_USER_ONE, SIMPLE_USER_TWO);
        softly.assertAll();
    }
}