package pl.kotwica.postoffice.monolith.user.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class UtilUserServiceTest {

    private Authentication authentication;
    private SecurityContext securityContext;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UtilUserService utilUserService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void setUpRest() {
        this.authentication = Mockito.mock(Authentication.class);
        this.securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(this.authentication);
        SecurityContextHolder.setContext(this.securityContext);
    }

    @Test
    public void userPresentIfLogged() {
        //given
        when(authentication.getName()).thenReturn(SIMPLE_EMAIL);
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));

        //when
        Optional<User> getUserMethodResult = utilUserService.getLoggedUser();

        //then
        assertThat(getUserMethodResult.get()).isEqualTo(SIMPLE_USER_ONE);
    }

    @Test
    public void userNotPresentIfNotLogged() {
        //given
        when(authentication.getName()).thenReturn("");
        when(userRepository.findUserByEmail("")).thenReturn(Optional.empty());

        //when
        Optional<User> getUserMethodResult = utilUserService.getLoggedUser();

        //then
        assertThat(getUserMethodResult).isEmpty();
    }
}