package pl.kotwica.postoffice.monolith.user.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import pl.kotwica.postoffice.monolith.testUtils.CommonControllerIntegrationTest;
import pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.kotwica.postoffice.monolith.testUtils.IntegrationTestData.*;

public class UserControllerIntegrationTest extends CommonControllerIntegrationTest {

    private final static String REGISTRATION_FORM_TEMPLATE = "{\n" +
            "    \"firstName\": \"First\",\n" +
            "    \"lastName\": \"Last\",\n" +
            "    \"email\": \"%s\",\n" +
            "    \"password\": \"pass\",\n" +
            "    \"passwordRep\": \"pass\"\n" +
            "}";

    @BeforeAll
    public static void init(@Autowired IntegrationTestData integrationTestData) {
        integrationTestData.addStandardUser();
    }

    @Test
    void gettingAllUsersCauseOkStatusWithJsonListOfUser() throws Exception {
        performProtectedGetRequest("/users", logStandardUser(), "")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[*].email", hasItem(LOGGED_USER_EMAIL)));
    }

    @Test
    void successfullyRegisterCauseOkStatusWithSuccessJson() throws Exception {
        this.mockMvc.perform(
                post("/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format(REGISTRATION_FORM_TEMPLATE, NOT_EXISTING_USER_EMAIL)))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"success\": true, " +
                                "\"message\": \"User registered\", " +
                                "\"errors\": null}"
                ));
    }

    @Test
    void formErrorsCauseBadRequestWithNoSuccessJson() throws Exception {
        this.mockMvc.perform(
                post("/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format(REGISTRATION_FORM_TEMPLATE, "")))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(
                        "{\"message\":\"Not valid form\"," +
                                "\"errors\":[{\"key\":\"registerForm\",\"value\":\"must not be empty\"}]," +
                                "\"success\":false}"
                ));
    }

    @Test
    void existingUserDuringRegistrationCauseConflictWithNoSuccessJson() throws Exception {
        this.mockMvc.perform(
                post("/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format(REGISTRATION_FORM_TEMPLATE, LOGGED_USER_EMAIL)))
                .andExpect(status().isConflict())
                .andExpect(content().json(
                        "{\"message\":\"User exists\"," +
                                "\"errors\": null," +
                                "\"success\":false}"
                ));
    }
}