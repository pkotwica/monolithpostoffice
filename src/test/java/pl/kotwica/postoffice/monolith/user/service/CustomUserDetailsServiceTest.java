package pl.kotwica.postoffice.monolith.user.service;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.SIMPLE_EMAIL;
import static pl.kotwica.postoffice.monolith.testUtils.TestData.SIMPLE_USER_ONE;

@RunWith(MockitoJUnitRunner.class)
public class CustomUserDetailsServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CustomUserDetailsService customUserDetailsService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void userDetailsPresentIfUserExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.of(SIMPLE_USER_ONE));

        //when
        UserDetails userDetailsByEmailMethodResult = customUserDetailsService.loadUserByUsername(SIMPLE_EMAIL);

        //then
        assertThat(userDetailsByEmailMethodResult).isEqualTo(SIMPLE_USER_ONE);
    }

    @Test
    public void throwUserNotFoundExceptionIfUserNotExists() {
        //given
        when(userRepository.findUserByEmail(SIMPLE_EMAIL)).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> customUserDetailsService.loadUserByUsername(SIMPLE_EMAIL))
                .isInstanceOf(UsernameNotFoundException.class).hasMessageContaining("User by email not found: " + SIMPLE_EMAIL);
    }
}