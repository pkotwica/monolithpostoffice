package pl.kotwica.postoffice.monolith.map.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "countries")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String name;
}
