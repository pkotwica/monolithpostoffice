package pl.kotwica.postoffice.monolith.map.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "postcodes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Postcode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;
}
