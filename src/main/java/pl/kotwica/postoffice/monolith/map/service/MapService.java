package pl.kotwica.postoffice.monolith.map.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.map.model.*;
import pl.kotwica.postoffice.monolith.map.repo.DistanceRepository;
import pl.kotwica.postoffice.monolith.map.repo.RoadRepository;

import java.util.Optional;

@Service
public class MapService {

    private final Integer UNKNOWN_DISTANCE = 0;

    private final DistanceRepository distanceRepository;
    private final RoadRepository roadRepository;

    public MapService(DistanceRepository distanceRepository, RoadRepository roadRepository) {
        this.distanceRepository = distanceRepository;
        this.roadRepository = roadRepository;
    }

    public Integer getDistance(String sourcePostCode, String destinationPostCode) {
        Optional<Distance> possibleDistanceOne = distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(sourcePostCode, destinationPostCode);
        Optional<Distance> possibleDistanceTwo = distanceRepository.findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(destinationPostCode, sourcePostCode);

        return possibleDistanceOne.map(Distance::getDistance).orElseGet(() -> possibleDistanceTwo.map(Distance::getDistance).orElse(UNKNOWN_DISTANCE));
    }

    public boolean checkAddressExistence(AddressExistenceForm addressExistenceForm) {
        return checkIfRoadAndPostcodeIsOk(addressExistenceForm);
    }

    private boolean checkIfRoadAndPostcodeIsOk(AddressExistenceForm addressExistenceForm) {
        Optional<Road> road = roadRepository.findFirstByNameAndPostcode_Code(addressExistenceForm.getRoad(), addressExistenceForm.getPostcode());

        return road.map(r -> {
            if(checkIfNumberIsOk(r, addressExistenceForm.getNumber())) {
                return checkIfCityIsOk(r.getPostcode().getCity(), addressExistenceForm);
            } else {
                return false;
            }
        }).orElse(false);
    }

    private boolean checkIfNumberIsOk(Road road, Integer number) {
        return (number >= road.getMinNumber()) && (number <= road.getMaxNumber());
    }

    private boolean checkIfCityIsOk(City city, AddressExistenceForm addressExistenceForm) {
        if(city.getName().equalsIgnoreCase(addressExistenceForm.getCity())) {
            return checkIfCountryIsOk(city.getCountry(), addressExistenceForm);
        } else {
            return false;
        }
    }

    private boolean checkIfCountryIsOk(Country country, AddressExistenceForm addressExistenceForm) {
        return country.getName().equalsIgnoreCase(addressExistenceForm.getCountry());
    }
}
