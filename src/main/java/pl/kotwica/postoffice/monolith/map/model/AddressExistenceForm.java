package pl.kotwica.postoffice.monolith.map.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddressExistenceForm {

    public static AddressExistenceForm of(ShipmentFormAddress shipmentFormAddress) {
        return new AddressExistenceForm(
                shipmentFormAddress.getCountry(),
                shipmentFormAddress.getCity(),
                shipmentFormAddress.getPostcode(),
                shipmentFormAddress.getRoad(),
                shipmentFormAddress.getNumber()
        );
    }

    private String country;
    private String city;
    private String postcode;
    private String road;
    private Integer number;
}
