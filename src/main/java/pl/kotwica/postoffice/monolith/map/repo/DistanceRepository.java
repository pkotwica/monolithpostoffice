package pl.kotwica.postoffice.monolith.map.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.map.model.Distance;

import java.util.Optional;

@Repository
public interface DistanceRepository extends JpaRepository<Distance, Integer> {
    Optional<Distance> findFirstByPostcodeOne_CodeAndPostcodeTwo_Code(String postcodeOne, String postcodeTwo);
}
