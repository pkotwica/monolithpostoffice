package pl.kotwica.postoffice.monolith.map.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.map.model.Road;

import java.util.Optional;

@Repository
public interface RoadRepository extends JpaRepository<Road, Integer> {
    Optional<Road> findFirstByNameAndPostcode_Code(String roadName, String postcode);
}
