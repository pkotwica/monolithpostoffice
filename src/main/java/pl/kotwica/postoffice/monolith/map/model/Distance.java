package pl.kotwica.postoffice.monolith.map.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "distances")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Distance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postcodes_one_id")
    private Postcode postcodeOne;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postcodes_two_id")
    private Postcode postcodeTwo;

    private Integer distance;
}
