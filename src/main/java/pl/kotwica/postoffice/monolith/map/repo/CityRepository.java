package pl.kotwica.postoffice.monolith.map.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kotwica.postoffice.monolith.map.model.City;

public interface CityRepository extends JpaRepository<City, Integer> {
}
