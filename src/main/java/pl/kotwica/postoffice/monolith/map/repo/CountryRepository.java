package pl.kotwica.postoffice.monolith.map.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.map.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
}
