package pl.kotwica.postoffice.monolith.map.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kotwica.postoffice.monolith.map.model.Postcode;

import java.util.Optional;

public interface PostcodeRepository extends JpaRepository<Postcode, Integer> {
    Optional<Postcode> findByCode(String code);
}
