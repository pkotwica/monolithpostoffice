package pl.kotwica.postoffice.monolith.map.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "roads")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Road {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private Integer minNumber;
    private Integer maxNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postcode_id")
    private Postcode postcode;
}
