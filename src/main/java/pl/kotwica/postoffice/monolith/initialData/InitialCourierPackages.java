package pl.kotwica.postoffice.monolith.initialData;

import org.springframework.stereotype.Component;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.Courier;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierRepository;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.packages.repo.PackageRepository;
import pl.kotwica.postoffice.monolith.packages.repo.TypedAddressRepository;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Component
public class InitialCourierPackages {

    private final CourierPackageRepository courierPackageRepository;
    private final UserRepository userRepository;
    private final TypedAddressRepository typedAddressRepository;
    private final PackageRepository packageRepository;
    private final CourierRepository courierRepository;
    private final PasswordService passwordEncoder;

    private static final List<TypedAddress> initialTypedAddresses = Arrays.asList(
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email1@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email2@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email3@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email4@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email5@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email6@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email7@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email8@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email9@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email10@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email11@email.com", "Country", "City", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "Doe", "123123123", "email12@email.com", "Country", "City", "10-100", "Road", 1))
    );

    private static final ShipmentForm shipmentFormCourier = new ShipmentForm(10, new PackageSize(3, 3, 3, 10));

    private static final Courier courierOne = Courier.of("Jan", "Blachowicz", 10);

    public InitialCourierPackages(CourierPackageRepository courierPackageRepository, UserRepository userRepository, TypedAddressRepository typedAddressRepository, PackageRepository packageRepository, CourierRepository courierRepository, PasswordService passwordEncoder) {
        this.courierPackageRepository = courierPackageRepository;
        this.userRepository = userRepository;
        this.typedAddressRepository = typedAddressRepository;
        this.packageRepository = packageRepository;
        this.courierRepository = courierRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void load() {
        Courier courier = addCourier(courierOne);
        User user = addUser(new RegisterForm("Name", "CourierPackage", "email@courierpackage.com", "pass", "pass"));
        Package aPackage;

        aPackage = addPackage(user, initialTypedAddresses.get(0), initialTypedAddresses.get(1), shipmentFormCourier);
        courierPackageRepository.save(CourierPackage.newPackage(BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), aPackage, courier));

        aPackage = addPackage(user, initialTypedAddresses.get(2), initialTypedAddresses.get(3), shipmentFormCourier);
        courierPackageRepository.save(CourierPackage.newPackage(BigDecimal.valueOf(Long.parseLong("25")), LocalDateTime.now().plusDays(4), aPackage, courier));

        aPackage = addPackage(user, initialTypedAddresses.get(4), initialTypedAddresses.get(5), shipmentFormCourier);
        courierPackageRepository.save(CourierPackage.newPackage(BigDecimal.valueOf(Long.parseLong("35")), LocalDateTime.now().plusDays(5), aPackage, courier));

        aPackage = addPackage(user, initialTypedAddresses.get(6), initialTypedAddresses.get(7), shipmentFormCourier);
        courierPackageRepository.save(CourierPackage.newPackage(BigDecimal.valueOf(Long.parseLong("50")), LocalDateTime.now().plusDays(6), aPackage, courier));
    }

    private User addUser(RegisterForm registerForm) {
        return userRepository.save(User.of(registerForm, passwordEncoder.encode(registerForm.getPassword())));
    }

    private Package addPackage(User user, TypedAddress typedAddress, TypedAddress typedAddress1, ShipmentForm shipmentForm) {
        typedAddress = typedAddressRepository.save(typedAddress);
        typedAddress1 = typedAddressRepository.save(typedAddress1);

        return packageRepository.save(Package.of(shipmentForm, user, typedAddress, typedAddress1));
    }

    private Courier addCourier(Courier courier) {
        return courierRepository.save(courier);
    }
}
