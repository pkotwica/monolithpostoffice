package pl.kotwica.postoffice.monolith.initialData;

import org.springframework.stereotype.Component;
import pl.kotwica.postoffice.monolith.map.model.*;
import pl.kotwica.postoffice.monolith.map.repo.*;

import java.util.Arrays;
import java.util.List;

@Component
public class InitialMapData {

    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private final PostcodeRepository postcodeRepository;
    private final RoadRepository roadRepository;
    private final DistanceRepository distanceRepository;

    public InitialMapData(CountryRepository countryRepository, CityRepository cityRepository, PostcodeRepository postcodeRepository, RoadRepository roadRepository, DistanceRepository distanceRepository) {
        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
        this.postcodeRepository = postcodeRepository;
        this.roadRepository = roadRepository;
        this.distanceRepository = distanceRepository;
    }

    public void load() {
        Country country = countryRepository.save(new Country(null, "Polska"));
        City city = cityRepository.save(new City(null, "Wroclaw", country));
        Postcode postcode = postcodeRepository.save(new Postcode(null, "WROC-1", city));
        roadRepository.save(new Road(null, "Legnicka", 1, 100, postcode));
        roadRepository.save(new Road(null, "Kamienna", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "WROC-2", city));
        roadRepository.save(new Road(null, "Komandorska", 1, 50, postcode));
        roadRepository.save(new Road(null, "Sztabowa", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "WROC-3", city));
        roadRepository.save(new Road(null, "Dluga", 1, 50, postcode));
        roadRepository.save(new Road(null, "Trzebnicka", 1, 10, postcode));

        city = cityRepository.save(new City(null, "Warszawa", country));
        postcode = postcodeRepository.save(new Postcode(null, "WARS-1", city));
        roadRepository.save(new Road(null, "Chmielna", 1, 100, postcode));
        roadRepository.save(new Road(null, "Zlota", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "WARS-2", city));
        roadRepository.save(new Road(null, "Krucza", 1, 50, postcode));
        roadRepository.save(new Road(null, "Srebrna", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "WARS-3", city));
        roadRepository.save(new Road(null, "Zgoda", 1, 50, postcode));
        roadRepository.save(new Road(null, "Bracka", 1, 10, postcode));

        city = cityRepository.save(new City(null, "Karkow", country));
        postcode = postcodeRepository.save(new Postcode(null, "KRAK-1", city));
        roadRepository.save(new Road(null, "Mikolajska", 1, 100, postcode));
        roadRepository.save(new Road(null, "Stolarska", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "KRAK-2", city));
        roadRepository.save(new Road(null, "Florianska", 1, 50, postcode));
        roadRepository.save(new Road(null, "Szpitalna", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "KRAK-3", city));
        roadRepository.save(new Road(null, "Miodowa", 1, 50, postcode));
        roadRepository.save(new Road(null, "Wielopole", 1, 10, postcode));

        city = cityRepository.save(new City(null, "Poznan", country));
        postcode = postcodeRepository.save(new Postcode(null, "POZN-1", city));
        roadRepository.save(new Road(null, "Garbary", 1, 100, postcode));
        roadRepository.save(new Road(null, "Wodna", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "POZN-2", city));
        roadRepository.save(new Road(null, "Klasztorna", 1, 50, postcode));
        roadRepository.save(new Road(null, "Wroclawska", 1, 10, postcode));
        postcode = postcodeRepository.save(new Postcode(null, "POZN-3", city));
        roadRepository.save(new Road(null, "Szkolna", 1, 50, postcode));
        roadRepository.save(new Road(null, "Ludgardy", 1, 10, postcode));

        loadDistances();
    }

    private void loadDistances() {
        Postcode postcodeWro1 = postcodeRepository.findByCode("WROC-1").get();
        Postcode postcodeWro2 = postcodeRepository.findByCode("WROC-2").get();
        Postcode postcodeWro3 = postcodeRepository.findByCode("WROC-3").get();
        Postcode postcodeWars1 = postcodeRepository.findByCode("WARS-1").get();
        Postcode postcodeWars2 = postcodeRepository.findByCode("WARS-2").get();
        Postcode postcodeWars3 = postcodeRepository.findByCode("WARS-3").get();
        Postcode postcodeKrak1 = postcodeRepository.findByCode("KRAK-1").get();
        Postcode postcodeKrak2 = postcodeRepository.findByCode("KRAK-2").get();
        Postcode postcodeKrak3 = postcodeRepository.findByCode("KRAK-3").get();
        Postcode postcodePozn1 = postcodeRepository.findByCode("POZN-1").get();
        Postcode postcodePozn2 = postcodeRepository.findByCode("POZN-2").get();
        Postcode postcodePozn3 = postcodeRepository.findByCode("POZN-3").get();

        //locals
        saveDistances(postcodeWro1, Arrays.asList(postcodeWro2, postcodeWro3), 10);
        saveDistances(postcodeWars1, Arrays.asList(postcodeWars2, postcodeWars3), 15);
        saveDistances(postcodeKrak1, Arrays.asList(postcodeKrak2, postcodeKrak3), 12);
        saveDistances(postcodePozn1, Arrays.asList(postcodePozn2, postcodePozn3), 8);

        //wro->war
        saveDistances(Arrays.asList(postcodeWro1, postcodeWro2, postcodeWro3), Arrays.asList(postcodeWars1, postcodeWars2, postcodeWars3), 200);
        //wro->krak
        saveDistances(Arrays.asList(postcodeWro1, postcodeWro2, postcodeWro3), Arrays.asList(postcodeKrak1, postcodeKrak2, postcodeKrak3), 250);
        //wro->pozn
        saveDistances(Arrays.asList(postcodeWro1, postcodeWro2, postcodeWro3), Arrays.asList(postcodePozn1, postcodePozn2, postcodePozn3), 90);
        //war->krak
        saveDistances(Arrays.asList(postcodeWars1, postcodeWars2, postcodeWars3), Arrays.asList(postcodeKrak1, postcodeKrak2, postcodeKrak3), 180);
        //war->pozn
        saveDistances(Arrays.asList(postcodeWars1, postcodeWars2, postcodeWars3), Arrays.asList(postcodePozn1, postcodePozn2, postcodePozn3), 150);
        //krak->pozn
        saveDistances(Arrays.asList(postcodeKrak1, postcodeKrak2, postcodeKrak3), Arrays.asList(postcodePozn1, postcodePozn2, postcodePozn3), 410);
    }

    private void saveDistances(Postcode postcode, List<Postcode> postcodeListDest, Integer distance) {
        postcodeListDest.forEach(p -> distanceRepository.save(new Distance(null, postcode, p, distance)));
    }

    private void saveDistances(List<Postcode> postcodeListSour, List<Postcode> postcodeListDest, Integer distance) {
        postcodeListSour.forEach(p1 -> postcodeListDest.forEach(p2 -> distanceRepository.save(new Distance(null, p1, p2, distance))));
    }
}
