package pl.kotwica.postoffice.monolith.initialData;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.Locker;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerBox;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackage;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackageStatus;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerBoxRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerRepository;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.packages.repo.PackageRepository;
import pl.kotwica.postoffice.monolith.packages.repo.TypedAddressRepository;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class InitialLockerPackages {

    private final LockerRepository lockerRepository;
    private final LockerBoxRepository lockerBoxRepository;
    private final LockerPackageRepository lockerPackageRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final PackageRepository packageRepository;
    private final TypedAddressRepository typedAddressRepository;


    private static final List<Locker> initialLockers = Arrays.asList(
            new Locker(null, "WRO-1", "LockerWroclaw1", "Poland", "Wroclaw", 1, "Road", "1", Collections.emptyList()),
            new Locker(null, "WRO-2", "LockerWroclaw2", "Poland", "Wroclaw", 1, "Road", "2", Collections.emptyList()),
            new Locker(null, "WRO-3", "DoeLockerWroclaw3", "Poland", "Wroclaw", 1, "Road", "3", Collections.emptyList()),
            new Locker(null, "WRO-4", "DoeLockerWroclaw4", "Poland", "Wroclaw", 1, "Road", "4", Collections.emptyList()),
            new Locker(null, "WR-1", "LockerWarszawa1", "Poland", "Warszawa", 2, "Road", "1", Collections.emptyList()),
            new Locker(null, "WR-2", "LockerWarszawa2", "Poland", "Warszawa", 2, "Road", "2", Collections.emptyList()),
            new Locker(null, "KR-1", "LockerKrakow1", "Poland", "Krakow", 3, "Road", "1", Collections.emptyList()),
            new Locker(null, "KR-2", "LockerKrakow2", "Poland", "Krakow", 3, "Road", "2", Collections.emptyList()),
            new Locker(null, "KR-3", "LockerKrakow3", "Poland", "Krakow", 3, "Road", "3", Collections.emptyList()),
            new Locker(null, "PO-1", "LockerPoznan1", "Poland", "Poznan", 4, "Road", "1", Collections.emptyList()),
            new Locker(null, "PO-2", "LockerPoznan2", "Poland", "Poznan", 4, "Road", "2", Collections.emptyList())
    );

    private static final List<TypedAddress> initialTypedAddresses = Arrays.asList(
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email1@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email2@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email3@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email4@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email5@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email6@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email7@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email8@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email9@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email10@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email11@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1)),
            TypedAddress.of(new ShipmentFormAddress("John", "DoeLocker", "123123123", "email12@emailLocker.com", "Polska", "Wroclaw", "10-100", "Road", 1))
    );

    private static final ShipmentForm shipmentFormLocker = new ShipmentForm(20, new PackageSize(3, 3, 3, 10));

    public InitialLockerPackages(LockerRepository lockerRepository, LockerBoxRepository lockerBoxRepository, LockerPackageRepository lockerPackageRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, PackageRepository packageRepository, TypedAddressRepository typedAddressRepository) {
        this.lockerRepository = lockerRepository;
        this.lockerBoxRepository = lockerBoxRepository;
        this.lockerPackageRepository = lockerPackageRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.packageRepository = packageRepository;
        this.typedAddressRepository = typedAddressRepository;
    }

    public void load() {
        initialLockers.forEach(lockerRepository::save);

        User user = addUser(new RegisterForm("Name", "LockerPackage", "email@locker.com", "pass", "pass"));
        Package aPackage;
        LockerBox sourceLockerBox;
        LockerBox destinationLockerBox;

        aPackage = addPackage(user, initialTypedAddresses.get(0), initialTypedAddresses.get(1), shipmentFormLocker);
        lockerPackageRepository.save(new LockerPackage(null, LockerPackageStatus.INTRODUCED.name(), BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), aPackage, null, null));

        aPackage = addPackage(user, initialTypedAddresses.get(2), initialTypedAddresses.get(3), shipmentFormLocker);
        lockerPackageRepository.save(new LockerPackage(null, LockerPackageStatus.INTRODUCED.name(), BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), aPackage, null, null));

        aPackage = addPackage(user, initialTypedAddresses.get(4), initialTypedAddresses.get(5), shipmentFormLocker);
        sourceLockerBox = lockerBoxRepository.save(new LockerBox(null, true, lockerRepository.findByCode("WRO-1").get()));
        destinationLockerBox = lockerBoxRepository.save(new LockerBox(null, true, lockerRepository.findByCode("WRO-2").get()));
        lockerPackageRepository.save(new LockerPackage(null, LockerPackageStatus.LEFT.name(), BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), aPackage, sourceLockerBox, destinationLockerBox));

        aPackage = addPackage(user, initialTypedAddresses.get(6), initialTypedAddresses.get(7), shipmentFormLocker);
        sourceLockerBox = lockerBoxRepository.save(new LockerBox(null, true, lockerRepository.findByCode("WRO-3").get()));
        destinationLockerBox = lockerBoxRepository.save(new LockerBox(null, true, lockerRepository.findByCode("WRO-4").get()));
        lockerPackageRepository.save(new LockerPackage(null, LockerPackageStatus.CONFIRMED.name(), BigDecimal.valueOf(Long.parseLong("30")), LocalDateTime.now().plusDays(3), aPackage, sourceLockerBox, destinationLockerBox));

        loadEmptyLockerBoxes();
    }

    private void loadEmptyLockerBoxes() {
        initialLockers.forEach(l -> {
            Optional<Locker> locker = lockerRepository.findByCode(l.getCode());
            for (int i = 0; i < 4; i++) {
                lockerBoxRepository.save(new LockerBox(null, false, locker.get()));
            }
        });
    }

    private User addUser(RegisterForm registerForm) {
        return userRepository.save(User.of(registerForm, passwordEncoder.encode(registerForm.getPassword())));
    }

    private Package addPackage(User user, TypedAddress typedAddress, TypedAddress typedAddress1, ShipmentForm shipmentForm) {
        typedAddress = typedAddressRepository.save(typedAddress);
        typedAddress1 = typedAddressRepository.save(typedAddress1);

        return packageRepository.save(Package.of(shipmentForm, user, typedAddress, typedAddress1));
    }
}
