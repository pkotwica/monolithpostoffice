package pl.kotwica.postoffice.monolith.initialData;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.addressBook.repo.AddressBookRepository;
import pl.kotwica.postoffice.monolith.addressBook.service.AddressBookService;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.service.UserService;

import java.util.Arrays;
import java.util.List;

@Component
@Profile({"dev", "docker", "test"})
public class InitialData {

    private final UserService userService;
    private final AddressBookRepository addressBookRepository;
    private final InitialCourierPackages initialCourierPackages;
    private final InitialLockerPackages initialLockerPackages;
    private final InitialMapData initialMapData;

    private static final List<RegisterForm> initialUsers = Arrays.asList(
            new RegisterForm("Piotr", "Kotwica", "piotr.kotwica@email.com", "pass", "pass"),
            new RegisterForm("Piotr1", "Kotwica", "piotr.kotwica1@email.com", "pass", "pass"),
            new RegisterForm("Piotr2", "Kotwica", "piotr.kotwica2@email.com", "pass", "pass"),
            new RegisterForm("Piotr3", "Kotwica", "piotr.kotwica3@email.com", "pass", "pass"),
            new RegisterForm("Piotr4", "Kotwica", "piotr.kotwica4@email.com", "pass", "pass"),
            new RegisterForm("Marcin", "Jarosinski", "marcin.jarosinski@email.com", "cin", "cin"),
            new RegisterForm("Marcin1", "Jarosinski", "marcin.jarosinski1@email.com", "cin", "cin"),
            new RegisterForm("Marcin2", "Jarosinski", "marcin.jarosinski2@email.com", "cin", "cin"),
            new RegisterForm("Marcin3", "Jarosinski", "marcin.jarosinski3@email.com", "cin", "cin"),
            new RegisterForm("Marcin4", "Jarosinski", "marcin.jarosinski4@email.com", "cin", "cin"),
            new RegisterForm("Marcin5", "Jarosinski", "marcin.jarosinski5@email.com", "cin", "cin"),
            new RegisterForm("Marcin6", "Jarosinski", "marcin.jarosinski6@email.com", "cin", "cin"),
            new RegisterForm("Marcin7", "Jarosinski", "marcin.jarosinski7@email.com", "cin", "cin"),
            new RegisterForm("Marcin8", "Jarosinski", "marcin.jarosinski8@email.com", "cin", "cin"),
            new RegisterForm("Marcin9", "Jarosinski", "marcin.jarosinski9@email.com", "cin", "cin"),
            new RegisterForm("Marcin10", "Jarosinski", "marcin.jarosinski10@email.com", "cin", "cin"),
            new RegisterForm("Marcin11", "Jarosinski", "marcin.jarosinski11@email.com", "cin", "cin"),
            new RegisterForm("Marcin12", "Jarosinski", "marcin.jarosinski12@email.com", "cin", "cin"),
            new RegisterForm("Marcin13", "Jarosinski", "marcin.jarosinski13@email.com", "cin", "cin"),
            new RegisterForm("Marcin14", "Jarosinski", "marcin.jarosinski14@email.com", "cin", "cin")
    );

    private static final List<ShipmentFormAddress> initialAddresses = Arrays.asList(
            new ShipmentFormAddress("John", "Doe", "123123123", "email@email1.com", "Country", "City", "10-100", "Road", 1),
            new ShipmentFormAddress("John1", "Doe1", "123123124", "email@email2.com", "Country1", "City1", "10-1001", "Road", 11),
            new ShipmentFormAddress("John2", "Doe2", "123123125", "email@email3.com", "Country2", "City2", "10-1002", "Road", 12),
            new ShipmentFormAddress("John3", "Doe3", "123123126", "email@email4.com", "Country3", "City3", "10-1003", "Road", 13),
            new ShipmentFormAddress("John4", "Doe4", "123123127", "email@email5.com", "Country4", "City4", "10-1004", "Road", 14)
    );

    public InitialData(UserService userService, AddressBookService addressBookService, AddressBookRepository addressBookRepository, InitialCourierPackages initialCourierPackages, InitialLockerPackages initialLockerPackages, InitialMapData initialMapData) {
        this.userService = userService;
        this.addressBookRepository = addressBookRepository;
        this.initialCourierPackages = initialCourierPackages;
        this.initialLockerPackages = initialLockerPackages;
        this.initialMapData = initialMapData;
    }

    public void loadInitialData() {
        initialUsers.forEach(userService::addNewUser);
        initialAddresses.forEach(a -> addressBookRepository.save(Address.of(a, userService.getUserByEmail("piotr.kotwica@email.com").get())));

        initialCourierPackages.load();
        initialLockerPackages.load();
        initialMapData.load();
    }
}
