package pl.kotwica.postoffice.monolith.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserById(long id);

    Optional<User> findUserByEmail(String email);

    List<User> findAllByOrderByLastName();

    List<User> findUsersByEmail(String email);

    Optional<User> findByToken_TokenCodeAndToken_ExpirationDateGreaterThan(String token, LocalDateTime date);
}
