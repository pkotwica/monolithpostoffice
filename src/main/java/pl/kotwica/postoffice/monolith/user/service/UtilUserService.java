package pl.kotwica.postoffice.monolith.user.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.util.Optional;

@Service
public class UtilUserService {
    private final UserRepository userRepository;

    public UtilUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getLoggedUser() {
        return userRepository.findUserByEmail(getLoggedUserEmail());
    }

    public String getLoggedUserEmail() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
