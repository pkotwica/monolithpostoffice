package pl.kotwica.postoffice.monolith.user.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kotwica.postoffice.monolith.common.web.CommonRequestResponse;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("api/users")
    public List<User> getUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("register")
    public ResponseEntity<CommonRequestResponse> registerNewUser(@RequestBody @Valid RegisterForm registerForm, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(CommonRequestResponse.of(false, "Not valid form", bindingResult));
        } else {
            return userService.addNewUser(registerForm) ?
                    ResponseEntity.ok().body(CommonRequestResponse.of(true, "User registered")) :
                    ResponseEntity.status(HttpStatus.CONFLICT).body(CommonRequestResponse.of(false, "User exists"));
        }
    }
}
