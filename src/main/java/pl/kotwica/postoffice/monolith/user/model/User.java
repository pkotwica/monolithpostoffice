package pl.kotwica.postoffice.monolith.user.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String firstName;
    private String lastName;
    private String pass;
    @Column(unique = true)
    private String email;

    @OneToOne(orphanRemoval = true)
    private AuthToken token;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Address> addresses;


    public static User of(RegisterForm registerForm, String pass) {
        return new User(registerForm, pass);
    }

    private User(RegisterForm registerForm, String pass) {
        this.pass = pass;
        this.firstName = registerForm.getFirstName();
        this.lastName = registerForm.getLastName();
        this.email = registerForm.getEmail();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public String getPassword() {
        return this.pass;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
