package pl.kotwica.postoffice.monolith.user.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.user.model.RegisterForm;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final static String EMAIL_ALERT = "EMAIL_ERR";
    private final static String LOGIN_ALERT = "LOGIN_ERR";

    private final UserRepository userRepository;
    private final PasswordService passwordEncoder;

    public UserService(UserRepository userRepository, PasswordService passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean addNewUser(RegisterForm registerForm) {
        if (userRepository.findUserByEmail(registerForm.getEmail()).isPresent()) {
            return false;
        }

        userRepository.save(User.of(registerForm, passwordEncoder.encode(registerForm.getPassword())));
        return true;
    }

    public Optional<User> getUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
