package pl.kotwica.postoffice.monolith.common.web;

import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class CommonRequestResponse extends AbstractRequestResponse {

    public CommonRequestResponse(boolean isSuccess, String message, List<Pair<String, String>> errors) {
        super(isSuccess, message, errors);
    }

    public static CommonRequestResponse of(boolean isSuccess, String message) {
        return new CommonRequestResponse(isSuccess, message, null);
    }

    public static CommonRequestResponse of(boolean isSuccess, String message, BindingResult bindingResult) {
        return new CommonRequestResponse(isSuccess, message, parseErrors(bindingResult));
    }
}
