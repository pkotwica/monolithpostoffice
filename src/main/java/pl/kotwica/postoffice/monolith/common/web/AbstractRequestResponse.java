package pl.kotwica.postoffice.monolith.common.web;

import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public abstract class AbstractRequestResponse {
    protected boolean isSuccess;
    protected String message;
    protected List<Pair<String, String>> errors;

    public AbstractRequestResponse(boolean isSuccess, String message, List<Pair<String, String>> errors) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.errors = errors;
    }

    public AbstractRequestResponse() {
    }

    protected static List<Pair<String, String>> parseErrors(BindingResult bindingResult) {
        return bindingResult.getAllErrors()
                .stream()
                .map(e -> new Pair<>(e.getObjectName(), e.getDefaultMessage()))
                .collect(Collectors.toList());
    }
}
