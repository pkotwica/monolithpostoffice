package pl.kotwica.postoffice.monolith.common.validator;

import pl.kotwica.postoffice.monolith.map.model.AddressExistenceForm;
import pl.kotwica.postoffice.monolith.map.service.MapService;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressExistsValidator implements ConstraintValidator<AddressExists, ShipmentFormAddress> {

    private final MapService mapService;

    public AddressExistsValidator(MapService mapService) {
        this.mapService = mapService;
    }

    @Override
    public boolean isValid(ShipmentFormAddress shipmentFormAddress, ConstraintValidatorContext context) {
        return mapService.checkAddressExistence(AddressExistenceForm.of(shipmentFormAddress));
    }
}
