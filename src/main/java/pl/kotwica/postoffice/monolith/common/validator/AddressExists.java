package pl.kotwica.postoffice.monolith.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = AddressExistsValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface AddressExists {
    String message() default "Given address does not exist.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
