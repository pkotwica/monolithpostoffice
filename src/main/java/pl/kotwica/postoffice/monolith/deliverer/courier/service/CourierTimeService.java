package pl.kotwica.postoffice.monolith.deliverer.courier.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.map.service.MapService;
import pl.kotwica.postoffice.monolith.packages.model.Package;

import java.time.LocalDateTime;

@Service
public class CourierTimeService {

    Integer STANDARD_DELIVERY_DAY_TIME = 1;

    Integer FIRST_DISTANCE_STAGE_KM = 100;
    Integer SECOND_DISTANCE_STAGE_KM = 200;
    Integer THIRD_DISTANCE_STAGE_KM = 400;
    Integer FIRST_DAY_DISTANCE_STAGE = 1;
    Integer SECOND_DAY_DISTANCE_STAGE = 2;
    Integer THIRD_DAY_DISTANCE_STAGE = 3;

    Integer FIRST_CAPACITY_STAGE = 30;
    Integer SECOND_CAPACITY_STAGE = 60;
    Integer THIRD_CAPACITY_STAGE = 90;
    Integer FIRST_DAY_CAPACITY_STAGE = 1;
    Integer SECOND_DAY_CAPACITY_STAGE = 2;
    Integer THIRD_DAY_CAPACITY_STAGE = 3;

    private final MapService mapService;

    public CourierTimeService(MapService mapService) {
        this.mapService = mapService;
    }

    public LocalDateTime getDeliveryTimePrediction(Package newPackage, Integer percentageCourierCapacity) {
        return LocalDateTime.now().plusDays(
                STANDARD_DELIVERY_DAY_TIME + calculateDaysByPackage(newPackage) + calculateDaysByCapacity(percentageCourierCapacity)
        );
    }

    private Integer calculateDaysByCapacity(Integer percentageCourierCapacity) {
        if(percentageCourierCapacity > THIRD_CAPACITY_STAGE)
            return THIRD_DAY_CAPACITY_STAGE;

        if(percentageCourierCapacity > SECOND_CAPACITY_STAGE)
            return SECOND_DAY_CAPACITY_STAGE;

        if(percentageCourierCapacity > FIRST_CAPACITY_STAGE)
            return FIRST_DAY_CAPACITY_STAGE;

        return 0;
    }

    private Integer calculateDaysByPackage(Package newPackage) {
        Integer distance = mapService.getDistance(newPackage.getSourceAddress().getPostcode(), newPackage.getDestinationAddress().getPostcode());

        if(distance > THIRD_DISTANCE_STAGE_KM)
            return THIRD_DAY_DISTANCE_STAGE;

        if(distance > SECOND_DISTANCE_STAGE_KM)
            return SECOND_DAY_DISTANCE_STAGE;

        if(distance > FIRST_DISTANCE_STAGE_KM)
            return FIRST_DAY_DISTANCE_STAGE;

        return 0;
    }
}
