package pl.kotwica.postoffice.monolith.deliverer.locker.model;

public interface SpareLocker {
    Locker getLocker();
}
