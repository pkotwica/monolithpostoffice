package pl.kotwica.postoffice.monolith.deliverer.courier.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "couriers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Courier {

    public static Courier of(String firstName, String lastName, Integer capacity) {
        return new Courier(
                null,
                firstName,
                lastName,
                capacity
        );
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String firstName;
    private String lastName;
    private Integer capacity;
}
