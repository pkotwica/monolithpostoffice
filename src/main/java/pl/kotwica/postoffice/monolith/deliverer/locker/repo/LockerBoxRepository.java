package pl.kotwica.postoffice.monolith.deliverer.locker.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerBox;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.SpareLocker;

import java.util.List;
import java.util.Optional;

public interface LockerBoxRepository extends JpaRepository<LockerBox, Integer> {

    List<SpareLocker> findDistinctByOccupiedIsFalseAndLocker_City(String city);
    Optional<LockerBox> findFirstByOccupiedIsFalseAndLocker_Id(Integer lockerId);
}

