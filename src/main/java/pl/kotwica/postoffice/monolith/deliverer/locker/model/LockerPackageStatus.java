package pl.kotwica.postoffice.monolith.deliverer.locker.model;

public enum LockerPackageStatus {
    INTRODUCED("Introduced", "Package introduced to system by client"),
    DECLINED("Declined", "Declined by client"),
    WAITING("Waiting", "Please wait for empty locker box in chosen lockers."),
    CONFIRMED("Confirmed", "Package confirmed by client. Waiting for leaving in source locker."),
    LEFT("Left", "Package left in source locker."),
    ON_THE_WAY("On the way", "Courier picked up package and it is on transition."),
    READY_TO_PICK("Ready to pick up", "Courier delivered package to destination locker."),
    PICKED_UP("Picked up", "Package picked from destination locker");

    private final String normalName;
    private final String description;

    LockerPackageStatus(String normalName, String description) {
        this.normalName = normalName;
        this.description = description;
    }

    public String normalName() {
        return normalName;
    }

    public String description() {
        return description;
    }

}
