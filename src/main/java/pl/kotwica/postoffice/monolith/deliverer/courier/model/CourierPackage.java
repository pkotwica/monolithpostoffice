package pl.kotwica.postoffice.monolith.deliverer.courier.model;


import lombok.*;
import pl.kotwica.postoffice.monolith.packages.model.Package;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "courier_packages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourierPackage {

    public static CourierPackage newPackage(BigDecimal price, LocalDateTime predictionDeliveryDate, Package aPackage, Courier courier) {
        return new CourierPackage(
                null,
                CourierPackageStatus.INTRODUCED.name(),
                price,
                predictionDeliveryDate,
                aPackage,
                courier
        );
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String status;
    private BigDecimal price;
    private LocalDateTime predictionDeliveryDate;

    @OneToOne
    private Package mainPackage;

    @ManyToOne
    @JoinColumn(name = "courier_id")
    private Courier courier;
}
