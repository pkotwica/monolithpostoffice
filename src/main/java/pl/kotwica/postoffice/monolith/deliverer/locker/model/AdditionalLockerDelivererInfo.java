package pl.kotwica.postoffice.monolith.deliverer.locker.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.kotwica.postoffice.monolith.deliverer.model.AdditionalDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;

import java.util.List;

@Getter
@Setter
public class AdditionalLockerDelivererInfo extends AdditionalDelivererInfo {
    private AdditionalLockerDelivererInfo(DelivererInfoData delivererInfoData, LockerPackageStatus lockerPackageStatus, List<LockerDTO> sourceLockers, List<LockerDTO> destinationLockers) {
        super(delivererInfoData);
        this.lockerPackageStatus = lockerPackageStatus;
        this.sourceLockers = sourceLockers;
        this.destinationLockers = destinationLockers;
    }

    public static AdditionalLockerDelivererInfo of(DelivererInfoData delivererInfoData, LockerPackageStatus lockerPackageStatus, List<LockerDTO> sourceLockers, List<LockerDTO> destinationLockers) {
        return new AdditionalLockerDelivererInfo(delivererInfoData, lockerPackageStatus, sourceLockers, destinationLockers);
    }

    public static AdditionalLockerDelivererInfo of(DelivererInfoData delivererInfoData, String lockerPackageStatus, List<LockerDTO> sourceLockers, List<LockerDTO> destinationLockers) {
        return AdditionalLockerDelivererInfo.of(delivererInfoData, LockerPackageStatus.valueOf(lockerPackageStatus), sourceLockers, destinationLockers);
    }

    private LockerPackageStatus lockerPackageStatus;
    private List<LockerDTO> sourceLockers;
    private List<LockerDTO> destinationLockers;
}
