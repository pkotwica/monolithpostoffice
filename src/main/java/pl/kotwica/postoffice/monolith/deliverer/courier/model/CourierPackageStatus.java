package pl.kotwica.postoffice.monolith.deliverer.courier.model;

public enum CourierPackageStatus {
    INTRODUCED("Introduced", "Package introduced to system by client"),
    DECLINED("Declined", "Declined by client"),
    CONFIRMED("Confirmed", "Package confirmed by client. Waiting for courier."),
    ON_THE_WAY("On the way", "Courier picked up package and it is on transition."),
    DELIVERED("Delivered", "Delivered to destination place");

    private final String normalName;
    private final String description;

    CourierPackageStatus(String normalName, String description) {
        this.normalName = normalName;
        this.description = description;
    }

    public String normalName() {
        return normalName;
    }

    public String description() {
        return description;
    }
}
