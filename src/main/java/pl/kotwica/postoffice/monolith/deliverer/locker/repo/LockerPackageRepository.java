package pl.kotwica.postoffice.monolith.deliverer.locker.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackage;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface LockerPackageRepository extends JpaRepository<LockerPackage, Integer> {
    Optional<LockerPackage> findByMainPackage_Id(Integer mainPackageId);
    List<LockerPackage> findAllByMainPackage_Owner(User owner);
}
