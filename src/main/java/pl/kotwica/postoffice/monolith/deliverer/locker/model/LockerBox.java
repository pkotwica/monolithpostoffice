package pl.kotwica.postoffice.monolith.deliverer.locker.model;

import lombok.*;

import javax.persistence.*;

@Entity(name = "locker_boxes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LockerBox {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private boolean occupied;

    @ManyToOne
    @JoinColumn(name = "locker_id")
    private Locker locker;
}
