package pl.kotwica.postoffice.monolith.deliverer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AdditionalDelivererInfo {

    public AdditionalDelivererInfo(DelivererInfoData delivererInfoData) {
        this.delivererInfoData = delivererInfoData;
    }

    protected DelivererInfoData delivererInfoData;
}
