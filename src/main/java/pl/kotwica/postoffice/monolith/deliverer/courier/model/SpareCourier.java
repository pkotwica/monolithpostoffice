package pl.kotwica.postoffice.monolith.deliverer.courier.model;

public interface SpareCourier {
    Integer getId();
    Integer getCapacity();
}
