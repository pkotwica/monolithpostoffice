package pl.kotwica.postoffice.monolith.deliverer.courier.repo;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.Courier;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.SpareCourier;

import javax.swing.text.html.HTMLDocument;
import java.util.List;

public interface CourierRepository extends JpaRepository<Courier, Integer> {
    List<SpareCourier> findAllByCapacityGreaterThan(Integer capacity);
}
