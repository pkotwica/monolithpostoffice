package pl.kotwica.postoffice.monolith.deliverer.courier.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourierPackageRepository extends JpaRepository<CourierPackage, Integer> {
    Integer countAllByCourier_IdAndStatusIn(Integer courierId, List<String> status);
    Optional<CourierPackage> findByMainPackage_Id(Integer aPackageID);
    List<CourierPackage> findAllByMainPackage_Owner(User owner);
}
