package pl.kotwica.postoffice.monolith.deliverer.locker.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity(name = "lockers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Locker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private String code;
    @Column(unique = true)
    private String name;
    private String country;
    private String city;
    private Integer cityId;
    private String road;
    private String number;
//    private Integer size;

    @OneToMany(mappedBy = "locker", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<LockerBox> lockerBoxes;
}
