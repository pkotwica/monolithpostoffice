package pl.kotwica.postoffice.monolith.deliverer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class DelivererInfoData {
    private final Integer delivererID;
    private final String delivererName;
    private final boolean isActive;
}
