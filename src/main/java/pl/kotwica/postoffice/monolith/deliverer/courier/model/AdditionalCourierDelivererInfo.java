package pl.kotwica.postoffice.monolith.deliverer.courier.model;

import lombok.Getter;
import lombok.Setter;
import pl.kotwica.postoffice.monolith.deliverer.model.AdditionalDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;

@Getter
@Setter
public class AdditionalCourierDelivererInfo extends AdditionalDelivererInfo {

    private AdditionalCourierDelivererInfo(DelivererInfoData delivererInfoData, CourierPackageStatus courierPackageStatus) {
        super(delivererInfoData);
        this.courierPackageStatus = courierPackageStatus;
    }

    public static AdditionalCourierDelivererInfo of(DelivererInfoData delivererInfoData, CourierPackageStatus courierPackageStatus) {
        return new AdditionalCourierDelivererInfo(delivererInfoData, courierPackageStatus);
    }

    public static AdditionalCourierDelivererInfo of(DelivererInfoData delivererInfoData, String courierPackageStatusName) {
        return AdditionalCourierDelivererInfo.of(delivererInfoData, CourierPackageStatus.valueOf(courierPackageStatusName));
    }

    private CourierPackageStatus courierPackageStatus;
}
