package pl.kotwica.postoffice.monolith.deliverer.courier.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.*;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.courier.repo.CourierRepository;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.deliverer.service.DelivererService;
import pl.kotwica.postoffice.monolith.packages.model.ConfirmShipForm;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.PackageFeedback;
import pl.kotwica.postoffice.monolith.packages.model.PackageInfo;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourierDelivererService implements DelivererService {

    private final CourierRepository courierRepository;
    private final CourierPackageRepository courierPackageRepository;
    private final CourierPriceService courierPriceService;
    private final CourierTimeService courierTimeService;

    @Value("${monolith.deliverer.service.courier.active}")
    private boolean IS_SERVICE_ACTIVE;
    private final String SERVICE_NAME = "Courier";
    private final Integer SERVICE_ID = 10;

    public CourierDelivererService(CourierRepository courierRepository, CourierPackageRepository courierPackageRepository, CourierPriceService courierPriceService, CourierTimeService courierTimeService) {
        this.courierRepository = courierRepository;
        this.courierPackageRepository = courierPackageRepository;
        this.courierPriceService = courierPriceService;
        this.courierTimeService = courierTimeService;
    }

    @Override
    public DelivererInfoData getInfoData() {
        return DelivererInfoData.builder()
                .delivererID(SERVICE_ID)
                .delivererName(SERVICE_NAME)
                .isActive(IS_SERVICE_ACTIVE)
                .build();
    }

    @Override
    public List<PackageInfo> getUsersPackages(User owner) {
        DelivererInfoData delivererInfoData = getInfoData();
        return courierPackageRepository.findAllByMainPackage_Owner(owner).stream()
                .map(p -> PackageInfo.of(p, AdditionalCourierDelivererInfo.of(delivererInfoData, p.getStatus())))
                .collect(Collectors.toList());
    }

    @Override
    public PackageFeedback getPackageInfo(Integer mainPackageID) {
        Optional<CourierPackage> courierPackage = courierPackageRepository.findByMainPackage_Id(mainPackageID);

        return courierPackage
                .map(p -> PackageFeedback.success(
                        p.getMainPackage(),
                        getInfoData(),
                        p.getPrice(),
                        p.getPredictionDeliveryDate(),
                        AdditionalCourierDelivererInfo.of(getInfoData(), p.getStatus())
                ))
                .orElse(PackageFeedback.sthWentWrongError());
    }

    @Override
    public PackageFeedback sendPackage(Package newPackage) {
        Optional<Courier> spareCourier = findSpareCourier();
        return spareCourier.map(courier -> saveNewCourierPackage(newPackage, courier)).orElseGet(PackageFeedback::insufficientError);
    }

    @Override
    public boolean confirmPackage(ConfirmShipForm confirmShipForm) {
        return courierPackageRepository.findByMainPackage_Id(confirmShipForm.getPackageID()).map(p -> confirmOrDeclineOnlyIntroducedPackage(p, confirmShipForm.isConfirm())).orElse(false);
    }

    private Optional<Courier> findSpareCourier() {
        List<SpareCourier> couriers = courierRepository.findAllByCapacityGreaterThan(0);

        Map<Integer, Integer> couriersWithSpareCapacity = couriers.stream().collect(Collectors.toMap(SpareCourier::getId, this::calculateSpareCapacity));

        Optional<Map.Entry<Integer, Integer>> courierIDWithHighestAmountOfSpareCapacity = couriersWithSpareCapacity.entrySet().stream()
                .filter(e -> e.getValue() > 0)
                .min(Map.Entry.comparingByValue());

        return courierIDWithHighestAmountOfSpareCapacity.flatMap(e -> courierRepository.findById(e.getKey()));
    }

    private Integer calculateSpareCapacity(SpareCourier spareCourier) {
        return spareCourier.getCapacity() - courierPackageRepository.countAllByCourier_IdAndStatusIn(spareCourier.getId(), getNamesOfBusyStatuses());
    }

    private Integer calculateSpareCapacity(Courier courier) {
        return courier.getCapacity() - courierPackageRepository.countAllByCourier_IdAndStatusIn(courier.getId(), getNamesOfBusyStatuses());
    }

    private List<String> getNamesOfBusyStatuses() {
        return Arrays.asList(CourierPackageStatus.CONFIRMED.name(), CourierPackageStatus.ON_THE_WAY.name());
    }

    private PackageFeedback saveNewCourierPackage(Package newPackage, Courier courier) {
        BigDecimal price = courierPriceService.calculatePriceForPackage(newPackage);
        Integer percentageCourierSpareCapacity = (int) Math.floor((courier.getCapacity() - calculateSpareCapacity(courier)) / (float) courier.getCapacity() * 100);
        LocalDateTime predictionDeliveryDate = courierTimeService.getDeliveryTimePrediction(newPackage, percentageCourierSpareCapacity);

        CourierPackage addedCourierPackage = courierPackageRepository.save(CourierPackage.newPackage(price, predictionDeliveryDate, newPackage, courier));

        return PackageFeedback.successShipPlacement(
                newPackage,
                getInfoData(),
                price,
                predictionDeliveryDate,
                AdditionalCourierDelivererInfo.of(getInfoData(), addedCourierPackage.getStatus())
        );
    }

    private boolean confirmOrDeclineOnlyIntroducedPackage(CourierPackage courierPackage, boolean isConfirmation) {
        if (courierPackage.getStatus().equals(CourierPackageStatus.INTRODUCED.name())) {
            String statusName = isConfirmation ? CourierPackageStatus.CONFIRMED.name() : CourierPackageStatus.DECLINED.name();
            courierPackage.setStatus(statusName);
            courierPackageRepository.save(courierPackage);
            return true;
        }
        return false;
    }
}
