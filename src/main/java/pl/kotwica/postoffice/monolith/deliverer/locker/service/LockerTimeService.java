package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.map.service.MapService;
import pl.kotwica.postoffice.monolith.packages.model.Package;

import java.time.LocalDateTime;

@Service
public class LockerTimeService {

    private final static Integer STANDARD_DELIVERY_DAY_TIME = 1;

    private final static Integer FIRST_DISTANCE_STAGE_KM = 100;
    private final static Integer SECOND_DISTANCE_STAGE_KM = 200;
    private final static Integer THIRD_DISTANCE_STAGE_KM = 400;
    private final static Integer FIRST_DAY_DISTANCE_STAGE = 2;
    private final static Integer SECOND_DAY_DISTANCE_STAGE = 3;
    private final static Integer THIRD_DAY_DISTANCE_STAGE = 4;

    private final MapService mapService;

    public LockerTimeService(MapService mapService) {
        this.mapService = mapService;
    }

    public LocalDateTime getDeliveryTimePrediction(Package newPackage) {
        return LocalDateTime.now().plusDays(
                STANDARD_DELIVERY_DAY_TIME + calculateDaysByPackage(newPackage)
        );
    }

    private Integer calculateDaysByPackage(Package newPackage) {
        Integer distance = mapService.getDistance(newPackage.getSourceAddress().getPostcode(), newPackage.getDestinationAddress().getPostcode());

        if(distance > THIRD_DISTANCE_STAGE_KM)
            return THIRD_DAY_DISTANCE_STAGE;

        if(distance > SECOND_DISTANCE_STAGE_KM)
            return SECOND_DAY_DISTANCE_STAGE;

        if(distance > FIRST_DISTANCE_STAGE_KM)
            return FIRST_DAY_DISTANCE_STAGE;

        return 0;
    }
}
