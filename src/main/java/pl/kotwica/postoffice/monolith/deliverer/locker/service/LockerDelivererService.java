package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.*;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerBoxRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerPackageRepository;
import pl.kotwica.postoffice.monolith.deliverer.locker.repo.LockerRepository;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.deliverer.service.DelivererService;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LockerDelivererService implements DelivererService {

    @Value("${monolith.deliverer.service.locker.active}")
    private boolean IS_SERVICE_ACTIVE;
    private final String SERVICE_NAME = "Locker";
    private final Integer SERVICE_ID = 20;

    private final LockerBoxRepository lockerBoxRepository;
    private final LockerPackageRepository lockerPackageRepository;
    private final LockerRepository lockerRepository;
    private final LockerPriceService lockerPriceService;
    private final LockerTimeService lockerTimeService;

    public LockerDelivererService(LockerBoxRepository lockerBoxRepository, LockerPackageRepository lockerPackageRepository, LockerRepository lockerRepository, LockerPriceService lockerPriceService, LockerTimeService lockerTimeService) {
        this.lockerBoxRepository = lockerBoxRepository;
        this.lockerPackageRepository = lockerPackageRepository;
        this.lockerRepository = lockerRepository;
        this.lockerPriceService = lockerPriceService;
        this.lockerTimeService = lockerTimeService;
    }

    @Override
    public DelivererInfoData getInfoData() {
        return DelivererInfoData.builder()
                .delivererID(SERVICE_ID)
                .delivererName(SERVICE_NAME)
                .isActive(IS_SERVICE_ACTIVE)
                .build();
    }

    @Override
    public List<PackageInfo> getUsersPackages(User owner) {
        DelivererInfoData delivererInfoData = getInfoData();

        return lockerPackageRepository.findAllByMainPackage_Owner(owner).stream()
                .map(p -> PackageInfo.of(
                        p,
                        AdditionalLockerDelivererInfo.of(
                                delivererInfoData,
                                p.getStatus(),
                                getLockersIfPresent(p.getSourceLockerBox()),
                                getLockersIfPresent(p.getDestinationLockerBox())
                        )))
                .collect(Collectors.toList());
    }

    @Override
    public PackageFeedback getPackageInfo(Integer mainPackageID) {
        Optional<LockerPackage> lockerPackage = lockerPackageRepository.findByMainPackage_Id(mainPackageID);

        return lockerPackage
                .map(p -> PackageFeedback.success(
                        p.getMainPackage(),
                        getInfoData(),
                        p.getPrice(),
                        p.getPredictionDeliveryDate(),
                        AdditionalLockerDelivererInfo.of(getInfoData(), p.getStatus(), getLockersIfPresent(p.getSourceLockerBox()), getLockersIfPresent(p.getDestinationLockerBox()))
                ))
                .orElse(PackageFeedback.sthWentWrongError());
    }

    @Override
    public PackageFeedback sendPackage(Package newPackage) {
        List<Locker> sourceLockersProposition = getAllSpareLockersForCity(newPackage.getSourceAddress().getCity());
        List<Locker> destinationLockersProposition = getAllSpareLockersForCity(newPackage.getDestinationAddress().getCity());

        if (!sourceLockersProposition.isEmpty() && !destinationLockersProposition.isEmpty()) {
            return introduceLockerPackage(newPackage, mapLockersForDTO(sourceLockersProposition), mapLockersForDTO(destinationLockersProposition));
        } else {
            return PackageFeedback.insufficientError();
        }
    }

    @Override
    public boolean confirmPackage(ConfirmShipForm confirmShipForm) {
        Optional<LockerPackage> lockerPackage = lockerPackageRepository.findByMainPackage_Id(confirmShipForm.getPackageID());

        if (confirmShipForm.isConfirm()) {
            Optional<Locker> sourceLockerCode = getLockerByGivenCode(confirmShipForm.getSourceLockerCode());
            Optional<Locker> destinationLockerCode = getLockerByGivenCode(confirmShipForm.getDestinationLockerCode());

            return confirmPackageByOptionals(lockerPackage, sourceLockerCode, destinationLockerCode);
        } else {
            return declinePackage(lockerPackage);
        }
    }

    private Optional<Locker> getLockerByGivenCode(String lockerCode) {
        return Optional.ofNullable(lockerCode).filter(s -> !s.isEmpty()).flatMap(lockerRepository::findByCode);
    }

    private List<LockerDTO> getLockersIfPresent(LockerBox lockerBox) {
        return lockerBox != null ?
                Collections.singletonList(LockerDTO.of(lockerBox.getLocker())) :
                Collections.emptyList();
    }

    private List<LockerDTO> mapLockersForDTO(List<Locker> lockers) {
        return lockers.stream().map(LockerDTO::of).collect(Collectors.toList());
    }

    private List<Locker> getAllSpareLockersForCity(String city) {
        List<SpareLocker> spareLockersByInterface = lockerBoxRepository.findDistinctByOccupiedIsFalseAndLocker_City(city.toLowerCase());
        return spareLockersByInterface.stream().map(SpareLocker::getLocker).collect(Collectors.toList());
    }

    private PackageFeedback introduceLockerPackage(Package newPackage, List<LockerDTO> sourceLockersProposition, List<LockerDTO> destinationLockersProposition) {
        BigDecimal price = lockerPriceService.calculatePriceForPackage(newPackage);
        LocalDateTime predictionDeliveryDate = lockerTimeService.getDeliveryTimePrediction(newPackage);

        LockerPackage addedLockerPackage = lockerPackageRepository.save(LockerPackage.newPackage(price, predictionDeliveryDate, newPackage));

        return PackageFeedback.successShipPlacement(
                newPackage,
                getInfoData(),
                price,
                predictionDeliveryDate,
                AdditionalLockerDelivererInfo.of(getInfoData(), addedLockerPackage.getStatus(), sourceLockersProposition, destinationLockersProposition)
        );
    }

    private boolean confirmPackageByOptionals(Optional<LockerPackage> lockerPackageOpt, Optional<Locker> sourceLockerOpt, Optional<Locker> destinationLockerOpt) {
        if (lockerPackageOpt.isPresent() && sourceLockerOpt.isPresent() && destinationLockerOpt.isPresent()) {
            LockerPackage lockerPackage = lockerPackageOpt.get();
            Locker sourceLocker = sourceLockerOpt.get();
            Locker destinationLocker = destinationLockerOpt.get();

            boolean isLockersValid = checkIfLockerBelongsToGivenCity(lockerPackage.getMainPackage().getSourceAddress(), sourceLocker) && checkIfLockerBelongsToGivenCity(lockerPackage.getMainPackage().getDestinationAddress(), destinationLocker);

            return isLockersValid && confirmPackageByValidData(lockerPackage, sourceLocker, destinationLocker);
        } else {
            return false;
        }
    }

    private boolean confirmPackageByValidData(LockerPackage lockerPackage, Locker sourceLocker, Locker destinationLocker) {
        if (!lockerPackage.getStatus().equals(LockerPackageStatus.INTRODUCED.name())) {
            return false;
        }

        Optional<LockerBox> sourceLockerBox = findSpareLockerBoxForLocker(sourceLocker);
        Optional<LockerBox> destinationLockerBox = findSpareLockerBoxForLocker(destinationLocker);

        LockerPackageStatus confirmedStatus = sourceLockerBox.isPresent() && destinationLockerBox.isPresent() ?
                LockerPackageStatus.CONFIRMED :
                LockerPackageStatus.WAITING;

        lockerPackage.setSourceLockerBox(sourceLockerBox.orElse(null));
        lockerPackage.setDestinationLockerBox(destinationLockerBox.orElse(null));
        lockerPackage.setStatus(confirmedStatus.name());

        lockerPackageRepository.save(lockerPackage);

        return true;
    }

    private Optional<LockerBox> findSpareLockerBoxForLocker(Locker locker) {
        return lockerBoxRepository.findFirstByOccupiedIsFalseAndLocker_Id(locker.getId());
    }

    private boolean checkIfLockerBelongsToGivenCity(TypedAddress address, Locker locker) {
        return address.getCity().equalsIgnoreCase(locker.getCity());
    }

    private boolean declinePackage(Optional<LockerPackage> lockerPackage) {
        lockerPackage.ifPresent(l -> {
            l.setStatus(LockerPackageStatus.DECLINED.name());
            lockerPackageRepository.save(l);
        });

        return lockerPackage.isPresent();
    }
}
