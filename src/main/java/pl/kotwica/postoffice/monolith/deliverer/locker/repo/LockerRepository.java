package pl.kotwica.postoffice.monolith.deliverer.locker.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.Locker;

import java.util.Optional;

@Repository
public interface LockerRepository extends JpaRepository<Locker, Integer> {
    Optional<Locker> findByCode(String code);
}
