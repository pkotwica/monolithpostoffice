package pl.kotwica.postoffice.monolith.deliverer.locker.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.packages.model.Package;

import java.math.BigDecimal;

@Service
public class LockerPriceService {
    private final BigDecimal START_PRICE = BigDecimal.valueOf(10);

    private final Integer FIRST_WEIGHT = 2;
    private final Integer SECOND_WEIGHT = 3;
    private final Integer THIRD_WEIGHT = 5;
    private final BigDecimal FIRST_WEIGH_PRICE = BigDecimal.valueOf(10);
    private final BigDecimal SECOND_WEIGH_PRICE = BigDecimal.valueOf(15);
    private final BigDecimal THIRD_WEIGHT_PRICE = BigDecimal.valueOf(30);

    private final Integer FIRST_V = 3000;
    private final Integer SECOND_V = 6000;
    private final Integer THIRD_V = 9000;
    private final BigDecimal FIRST_V_PRICE = BigDecimal.valueOf(20);
    private final BigDecimal SECOND_V_PRICE = BigDecimal.valueOf(30);
    private final BigDecimal THIRD_V_PRICE = BigDecimal.valueOf(55);

    public BigDecimal calculatePriceForPackage(Package newPackage) {
        return START_PRICE.add(calculatePriceForWeight(newPackage)).add(calculatePriceForV(newPackage));
    }

    private BigDecimal calculatePriceForV(Package newPackage) {
        int v = newPackage.getSize_x() * newPackage.getSize_y() * newPackage.getSize_z();

        if(v > THIRD_V)
            return THIRD_V_PRICE;

        if(v > SECOND_V)
            return SECOND_V_PRICE;

        if(v > FIRST_V)
            return FIRST_V_PRICE;

        return BigDecimal.ZERO;
    }

    private BigDecimal calculatePriceForWeight(Package newPackage) {
        Integer packageWeight = newPackage.getWeight();

        if(packageWeight > THIRD_WEIGHT)
            return THIRD_WEIGHT_PRICE;

        if(packageWeight > SECOND_WEIGHT)
            return SECOND_WEIGH_PRICE;

        if(packageWeight > FIRST_WEIGHT)
            return FIRST_WEIGH_PRICE;

        return BigDecimal.ZERO;
    }
}
