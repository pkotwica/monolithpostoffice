package pl.kotwica.postoffice.monolith.deliverer.locker.model;

import lombok.*;
import pl.kotwica.postoffice.monolith.packages.model.Package;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "locker_packages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LockerPackage {

    private LockerPackage(String status, BigDecimal price, LocalDateTime predictionDeliveryDate, Package mainPackage) {
        this.status = status;
        this.price = price;
        this.predictionDeliveryDate = predictionDeliveryDate;
        this.mainPackage = mainPackage;
    }

    public static LockerPackage newPackage(BigDecimal price, LocalDateTime predictionDeliveryDate, Package mainPackage) {
        return new LockerPackage(
                LockerPackageStatus.INTRODUCED.name(),
                price,
                predictionDeliveryDate,
                mainPackage
        );
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String status;
    private BigDecimal price;
    private LocalDateTime predictionDeliveryDate;

    @OneToOne
    private Package mainPackage;

    @ManyToOne
    @JoinColumn(name = "source_locker_box_id")
    private LockerBox sourceLockerBox;

    @ManyToOne
    @JoinColumn(name = "destination_locker_box_id")
    private LockerBox destinationLockerBox;
}
