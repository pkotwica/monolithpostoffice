package pl.kotwica.postoffice.monolith.deliverer.locker.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LockerDTO {

    public static LockerDTO of(Locker locker) {
        return new LockerDTO(
                locker.getCode(),
                locker.getName(),
                locker.getCountry(),
                locker.getCity(),
                locker.getCityId(),
                locker.getRoad(),
                locker.getNumber()
        );
    }

    private String code;
    private String name;
    private String country;
    private String city;
    private Integer cityId;
    private String road;
    private String number;
//    private Integer size;
}
