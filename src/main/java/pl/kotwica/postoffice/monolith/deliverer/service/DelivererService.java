package pl.kotwica.postoffice.monolith.deliverer.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.packages.model.ConfirmShipForm;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.PackageFeedback;
import pl.kotwica.postoffice.monolith.packages.model.PackageInfo;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.util.List;

@Service
public interface DelivererService {
    DelivererInfoData getInfoData();

    List<PackageInfo> getUsersPackages(User owner);

    PackageFeedback getPackageInfo(Integer mainPackageID);

    PackageFeedback sendPackage(Package newPackage);

    boolean confirmPackage(ConfirmShipForm confirmShipForm);
}
