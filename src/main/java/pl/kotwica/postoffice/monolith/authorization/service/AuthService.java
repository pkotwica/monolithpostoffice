package pl.kotwica.postoffice.monolith.authorization.service;


import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;
import pl.kotwica.postoffice.monolith.authorization.model.LoginBody;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.util.Optional;

public interface AuthService {
    Optional<AuthToken> generateAuthToken(LoginBody loginBody);

    Optional<User> findUserByToken(String tokenCode);
}
