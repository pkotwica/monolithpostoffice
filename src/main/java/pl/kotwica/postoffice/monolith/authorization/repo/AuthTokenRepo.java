package pl.kotwica.postoffice.monolith.authorization.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;

@Repository
public interface AuthTokenRepo extends JpaRepository<AuthToken, Long> {
}
