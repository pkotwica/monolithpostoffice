package pl.kotwica.postoffice.monolith.authorization.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;
import pl.kotwica.postoffice.monolith.authorization.model.LoginBody;
import pl.kotwica.postoffice.monolith.authorization.model.TokenResponse;
import pl.kotwica.postoffice.monolith.authorization.service.AuthService;

import java.util.Optional;

@RestController
@RequestMapping("token")
public class TokenController {

    private final AuthService authService;

    public TokenController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<?> getToken(@RequestBody final LoginBody loginBody) {
        Optional<AuthToken> token = authService.generateAuthToken(loginBody);
        if (!token.isPresent()) {
            return new ResponseEntity<>("Unauthorized. Wrong Credentials.", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(TokenResponse.of(token.get()), HttpStatus.OK);
    }
}
