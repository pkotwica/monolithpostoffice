package pl.kotwica.postoffice.monolith.authorization.service.impl;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.authorization.PasswordService;
import pl.kotwica.postoffice.monolith.authorization.model.AuthToken;
import pl.kotwica.postoffice.monolith.authorization.model.LoginBody;
import pl.kotwica.postoffice.monolith.authorization.repo.AuthTokenRepo;
import pl.kotwica.postoffice.monolith.authorization.service.AuthService;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.repo.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class DefaultAuthService implements AuthService {

    private final static int EXPIRATION_TIME_MINUTES = 30;

    private final UserRepository userRepository;
    private final AuthTokenRepo tokenRepository;
    private final PasswordService passwordEncoder;

    public DefaultAuthService(UserRepository userRepository, PasswordService passwordEncoder, AuthTokenRepo authTokenRepo) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = authTokenRepo;
    }

    public Optional<AuthToken> generateAuthToken(LoginBody loginBody) {
        Optional<User> optionalUser = userRepository.findUserByEmail(loginBody.getUsername());

        if(optionalUser.isPresent() && passwordEncoder.matches(loginBody.getPassword(), optionalUser.get().getPass())) {
            User user  = optionalUser.get();
            setTokenForUser(user);
            return Optional.of(user.getToken());
        }

        return Optional.empty();
    }

    public Optional<User> findUserByToken(String tokenCode) {
        return userRepository.findByToken_TokenCodeAndToken_ExpirationDateGreaterThan(tokenCode, LocalDateTime.now());
    }

    private void setTokenForUser(User user) {
        if(user.getToken() == null || user.getToken().getExpirationDate().isBefore(LocalDateTime.now())) {
            user.setToken(generateNewToken());
            tokenRepository.save(user.getToken());
            userRepository.save(user);
        } else {
            refreshToken(user.getToken());
            tokenRepository.save(user.getToken());
        }
    }

    private AuthToken generateNewToken() {
        String newToken = UUID.randomUUID().toString();
        return AuthToken.of(newToken, LocalDateTime.now().plusMinutes(EXPIRATION_TIME_MINUTES));
    }

    private void refreshToken(AuthToken token) {
        token.setExpirationDate(LocalDateTime.now().plusMinutes(EXPIRATION_TIME_MINUTES));
    }
}
