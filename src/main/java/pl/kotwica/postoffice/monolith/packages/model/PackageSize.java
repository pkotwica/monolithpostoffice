package pl.kotwica.postoffice.monolith.packages.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PackageSize {
    @NotNull
    private Integer x;
    @NotNull
    private Integer y;
    @NotNull
    private Integer z;
    @NotNull
    private Integer weight;
}
