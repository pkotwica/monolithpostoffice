package pl.kotwica.postoffice.monolith.packages.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.packages.model.Package;

@Repository
public interface PackageRepository extends JpaRepository<Package, Integer> {
}
