package pl.kotwica.postoffice.monolith.packages.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kotwica.postoffice.monolith.common.web.CommonRequestResponse;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.packages.model.ConfirmShipForm;
import pl.kotwica.postoffice.monolith.packages.model.PackageFeedback;
import pl.kotwica.postoffice.monolith.packages.model.PackageInfo;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentForm;
import pl.kotwica.postoffice.monolith.packages.service.PackageService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/packages")
public class PackageController {

    private final PackageService packageService;

    public PackageController(PackageService packageService) {
        this.packageService = packageService;
    }

    @GetMapping
    public List<PackageInfo> getPackages() {
        return packageService.getLoggedUserPackages();
    }

    @GetMapping("check-status")
    public ResponseEntity<PackageFeedback> getPackageInfo(@RequestParam Integer packageID) {
        PackageFeedback packageFeedback = packageService.getPackageInfo(packageID);

        return packageFeedback.isSuccess() ?
                ResponseEntity.ok(packageFeedback) :
                ResponseEntity.status(HttpStatus.CONFLICT).body(packageFeedback);
    }

    @GetMapping("deliverers")
    public List<DelivererInfoData> getPossibleDeliverers() {
        return packageService.getActiveDeliverersInfo();
    }

    @PostMapping("ship")
    public ResponseEntity<PackageFeedback> placeNewShipment(@RequestBody @Valid ShipmentForm shipmentForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(PackageFeedback.validationErrors(bindingResult));
        } else {
            PackageFeedback shipmentFeedback = packageService.placeShipment(shipmentForm);
            return shipmentFeedback.isSuccess() ?
                    ResponseEntity.ok().body(shipmentFeedback) :
                    ResponseEntity.status(HttpStatus.CONFLICT).body(shipmentFeedback);
        }
    }

    @PostMapping("confirm-ship")
    public ResponseEntity<CommonRequestResponse> confirmNewShipmentPlacement(@RequestBody ConfirmShipForm confirmShipForm) {
        return packageService.confirmShipment(confirmShipForm) ?
                ResponseEntity.ok(CommonRequestResponse.of(true, "Shipment confirmed successfully")) :
                ResponseEntity.status(HttpStatus.CONFLICT).body(CommonRequestResponse.of(false, "Wrong data"));
    }
}
