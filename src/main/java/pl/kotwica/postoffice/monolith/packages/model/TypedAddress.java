package pl.kotwica.postoffice.monolith.packages.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "typed_addresses")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TypedAddress {

    public static TypedAddress of(ShipmentFormAddress formAddress) {
        return new TypedAddress(
                null,
                formAddress.getFirstName(),
                formAddress.getLastName(),
                formAddress.getPhoneNumber(),
                formAddress.getEmail(),
                formAddress.getCountry(),
                formAddress.getCity(),
                formAddress.getPostcode(),
                formAddress.getRoad(),
                formAddress.getNumber()
        );
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String country;
    private String city;
    private String postcode;
    private String road;
    private Integer number;
}
