package pl.kotwica.postoffice.monolith.packages.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.packages.model.TypedAddress;

import java.util.List;

@Repository
public interface TypedAddressRepository extends JpaRepository<TypedAddress, Integer> {

    List<TypedAddress> findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(
            String firstName,
            String lastName,
            String phoneNumber,
            String email,
            String country,
            String city,
            String postcode,
            String road,
            Integer number
    );
}
