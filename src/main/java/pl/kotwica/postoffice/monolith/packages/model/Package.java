package pl.kotwica.postoffice.monolith.packages.model;

import lombok.*;
import pl.kotwica.postoffice.monolith.user.model.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "packages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Package {

    public static Package of(ShipmentForm shipmentForm, User owner, TypedAddress sourceAddress, TypedAddress destinationAddress) {
        PackageSize size = shipmentForm.getPackageSize();

        return new Package(
                null,
                sourceAddress,
                destinationAddress,
                owner,
                size.getX(),
                size.getY(),
                size.getZ(),
                size.getWeight(),
                LocalDateTime.now(),
                shipmentForm.getDelivererID()
        );
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "source_address_id")
    private TypedAddress sourceAddress;

    @ManyToOne
    @JoinColumn(name = "destination_address_id")
    private TypedAddress destinationAddress;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User owner;

    private Integer size_x;
    private Integer size_y;
    private Integer size_z;

    private Integer weight;

    private LocalDateTime sendingDate;
    private Integer delivererID;
}
