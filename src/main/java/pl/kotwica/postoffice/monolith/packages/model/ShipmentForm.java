package pl.kotwica.postoffice.monolith.packages.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ShipmentForm {
    public ShipmentForm(Integer delivererID, PackageSize packageSize) {
        this.delivererID = delivererID;
        this.packageSize = packageSize;
    }

    @NotNull
    @Valid
    private ShipmentFormAddress sourceAddress;
    @NotNull
    @Valid
    private ShipmentFormAddress destinationAddress;
    @NotNull
    @Valid
    private PackageSize packageSize;
    @NotNull
    private Integer delivererID;
}
