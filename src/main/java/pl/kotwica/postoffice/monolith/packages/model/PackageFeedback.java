package pl.kotwica.postoffice.monolith.packages.model;

import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;
import pl.kotwica.postoffice.monolith.common.web.AbstractRequestResponse;
import pl.kotwica.postoffice.monolith.deliverer.model.AdditionalDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PackageFeedback extends AbstractRequestResponse {
    public static final String FORM_VALIDATION_ERROR_MESSAGE = "Form validation error";
    public static final String SERVICE_ERROR_MESSAGE = "Chosen deliverer not exists or is unavailable";
    public static final String INSUFFICIENT_ERROR = "Chosen deliverer has insufficient resources, please choose another deliverer";
    public static final String SUCCESS_SHIP_MESSAGE = "Package successfully added - to confirm";
    public static final String SUCCESS_MESSAGE = "Success";
    public static final String STH_WENT_WRONG = "Something went wrong.";

    private PackageFeedback(boolean isSuccess, String message, List<Pair<String, String>> errors, Package packageInProgress, DelivererInfoData delivererInfoData, BigDecimal price, LocalDateTime predictionDeliveryDate, AdditionalDelivererInfo additionalDelivererInfo) {
        super(isSuccess, message, errors);
        this.packageInfo = new PackageInfo(
                packageInProgress,
                price,
                predictionDeliveryDate,
                additionalDelivererInfo
        );
    }

    private PackageFeedback(boolean isSuccess, String message, List<Pair<String, String>> errors) {
        super(isSuccess, message, errors);
    }

    public static PackageFeedback wrongService() {
        return new PackageFeedback(false, SERVICE_ERROR_MESSAGE, null);
    }

    public static PackageFeedback validationErrors(BindingResult bindingResult) {
        return new PackageFeedback(false, FORM_VALIDATION_ERROR_MESSAGE, parseErrors(bindingResult));
    }

    public static PackageFeedback insufficientError() {
        return new PackageFeedback(false, INSUFFICIENT_ERROR, null);
    }

    public static PackageFeedback sthWentWrongError() {
        return new PackageFeedback(false, STH_WENT_WRONG, null);
    }

    public static PackageFeedback successShipPlacement(Package packageInProgress, DelivererInfoData delivererInfoData, BigDecimal packagePrice, LocalDateTime predictionDeliveryDate, AdditionalDelivererInfo additionalDelivererInfo) {
        return new PackageFeedback(
                true,
                SUCCESS_SHIP_MESSAGE,
                null,
                packageInProgress,
                delivererInfoData,
                packagePrice,
                predictionDeliveryDate,
                additionalDelivererInfo
        );
    }

    public static PackageFeedback success(Package aPackage, DelivererInfoData delivererInfoData, BigDecimal packagePrice, LocalDateTime predictionDeliveryDate, AdditionalDelivererInfo additionalDelivererInfo) {
        return new PackageFeedback(
                true,
                SUCCESS_MESSAGE,
                null,
                aPackage,
                delivererInfoData,
                packagePrice,
                predictionDeliveryDate,
                additionalDelivererInfo
        );
    }

    private PackageInfo packageInfo;
}
