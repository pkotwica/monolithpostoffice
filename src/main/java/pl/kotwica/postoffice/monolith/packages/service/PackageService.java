package pl.kotwica.postoffice.monolith.packages.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;
import pl.kotwica.postoffice.monolith.deliverer.service.DelivererService;
import pl.kotwica.postoffice.monolith.packages.model.Package;
import pl.kotwica.postoffice.monolith.packages.model.*;
import pl.kotwica.postoffice.monolith.packages.repo.PackageRepository;
import pl.kotwica.postoffice.monolith.packages.repo.TypedAddressRepository;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.service.UtilUserService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PackageService {

    private final PackageRepository packageRepository;
    private final TypedAddressRepository typedAddressRepository;
    private final List<DelivererService> delivererServices;
    private final UtilUserService utilUserService;

    public PackageService(PackageRepository packageRepository, TypedAddressRepository typedAddressRepository, List<DelivererService> delivererServices, UtilUserService utilUserService) {
        this.delivererServices = delivererServices;
        this.packageRepository = packageRepository;
        this.typedAddressRepository = typedAddressRepository;
        this.utilUserService = utilUserService;
    }

    public List<DelivererInfoData> getActiveDeliverersInfo() {
        return delivererServices.stream().map(DelivererService::getInfoData).filter(DelivererInfoData::isActive).collect(Collectors.toList());
    }

    public List<PackageInfo> getLoggedUserPackages() {
        Optional<User> loggedUser = utilUserService.getLoggedUser();

        return loggedUser
                .map(user -> getActiveDeliverers().map(s -> s.getUsersPackages(user)).flatMap(Collection::stream).collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    @Transactional
    public PackageFeedback placeShipment(ShipmentForm shipmentForm) {
        Optional<User> loggedUser = utilUserService.getLoggedUser();

        if(loggedUser.isPresent()) {
            TypedAddress savedSourceAddress = saveNewTypedAddress(shipmentForm.getSourceAddress());
            TypedAddress savedDestinationAddress = saveNewTypedAddress(shipmentForm.getDestinationAddress());
            Package addedPackage = packageRepository.save(Package.of(shipmentForm, loggedUser.get(), savedSourceAddress, savedDestinationAddress));
            return handleNewPackageByDeliverer(addedPackage, shipmentForm.getDelivererID());
        } else {
            return PackageFeedback.sthWentWrongError();
        }
    }

    public boolean confirmShipment(ConfirmShipForm confirmShipForm) {
        Optional<Package> aPackage = findPackageById(confirmShipForm.getPackageID());

        if(aPackage.isPresent() && checkPackageBelongsToLoggedUser(aPackage)) {
            Optional<DelivererService> chosenDeliverer = getChosenDeliverer(aPackage.get().getDelivererID());
            return chosenDeliverer.map(s -> s.confirmPackage(confirmShipForm)).orElse(false);
        } else {
            return false;
        }
    }

    public PackageFeedback getPackageInfo(Integer packageID) {
        Optional<Package> aPackage = packageRepository.findById(packageID);

        if(aPackage.isPresent() && checkPackageBelongsToLoggedUser(aPackage)) {
            Optional<DelivererService> chosenDeliverer = getChosenDeliverer(aPackage.get().getDelivererID());
            return chosenDeliverer.map(d -> d.getPackageInfo(packageID)).orElse(PackageFeedback.sthWentWrongError());
        } else {
            return PackageFeedback.sthWentWrongError();
        }
    }

    private Optional<Package> findPackageById(Integer packageID) {
        return Optional.ofNullable(packageID).flatMap(packageRepository::findById);
    }

    private boolean checkPackageBelongsToLoggedUser(Optional<Package> aPackage) {
        Optional<User> loggedUser = utilUserService.getLoggedUser();

        if(aPackage.isPresent() && loggedUser.isPresent()) {
            return aPackage.get().getOwner().getId() == loggedUser.get().getId();
        }

        return false;
    }

    private TypedAddress saveNewTypedAddress(ShipmentFormAddress formAddress) {
        List<TypedAddress> existingAddresses = typedAddressRepository
                .findAllByFirstNameAndLastNameAndPhoneNumberAndEmailAndCountryAndCityAndPostcodeAndRoadAndNumber(
                        formAddress.getFirstName(),
                        formAddress.getLastName(),
                        formAddress.getPhoneNumber(),
                        formAddress.getEmail(),
                        formAddress.getCountry(),
                        formAddress.getCity(),
                        formAddress.getPostcode(),
                        formAddress.getRoad(),
                        formAddress.getNumber()
                );

        return existingAddresses.stream().findAny().orElseGet(() -> typedAddressRepository.save(TypedAddress.of(formAddress)));
    }

    private Optional<DelivererService> getChosenDeliverer(Integer serviceID) {
        return  getActiveDeliverers().filter(s -> s.getInfoData().getDelivererID().equals(serviceID)).findAny();
    }

    private PackageFeedback handleNewPackageByDeliverer(Package newPackage, Integer delivererID) {
        return getChosenDeliverer(delivererID).map(d -> d.sendPackage(newPackage)).orElseGet(PackageFeedback::wrongService);
    }

    private Stream<DelivererService> getActiveDeliverers() {
        return delivererServices.stream().filter(s -> s.getInfoData().isActive());
    }
}
