package pl.kotwica.postoffice.monolith.packages.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kotwica.postoffice.monolith.common.validator.AddressExists;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@AddressExists
public class ShipmentFormAddress {
    @NotEmpty
    @NotNull
    private String firstName;
    @NotEmpty
    @NotNull
    private String lastName;
    @NotEmpty
    @NotNull
    private String phoneNumber;
    @NotEmpty
    @NotNull
    private String email;
    @NotEmpty
    @NotNull
    private String country;
    @NotEmpty
    @NotNull
    private String city;
    @NotEmpty
    @NotNull
    private String postcode;
    @NotEmpty
    @NotNull
    private String road;
    @NotNull
    private Integer number;
}
