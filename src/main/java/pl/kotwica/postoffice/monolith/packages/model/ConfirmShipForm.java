package pl.kotwica.postoffice.monolith.packages.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ConfirmShipForm {
    private Integer packageID;
    private boolean confirm;
    private String sourceLockerCode;
    private String destinationLockerCode;
}
