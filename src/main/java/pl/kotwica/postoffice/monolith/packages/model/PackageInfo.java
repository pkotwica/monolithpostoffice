package pl.kotwica.postoffice.monolith.packages.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.AdditionalCourierDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.courier.model.CourierPackage;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.AdditionalLockerDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.locker.model.LockerPackage;
import pl.kotwica.postoffice.monolith.deliverer.model.AdditionalDelivererInfo;
import pl.kotwica.postoffice.monolith.deliverer.model.DelivererInfoData;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class PackageInfo {

    public PackageInfo(Package aPackage, BigDecimal price, LocalDateTime predictionDeliveryDate, AdditionalDelivererInfo additionalDelivererInfo) {
        this.id = aPackage.getId();
        this.sourceAddress = aPackage.getSourceAddress();
        this.destinationAddress = aPackage.getDestinationAddress();
        this.packageSize = new PackageSize(aPackage.getSize_x(), aPackage.getSize_y(), aPackage.getSize_z(), aPackage.getWeight());
        this.sendingDate = aPackage.getSendingDate();
        this.predictionDeliveryDate = predictionDeliveryDate;
        this.price = price;
        this.additionalDelivererInfo = additionalDelivererInfo;
    }

    public static PackageInfo of(CourierPackage courierPackage, AdditionalCourierDelivererInfo additionalDelivererInfo) {
        return new PackageInfo(
                courierPackage.getMainPackage(),
                courierPackage.getPrice(),
                courierPackage.getPredictionDeliveryDate(),
                additionalDelivererInfo
        );
    }

    public static PackageInfo of(LockerPackage lockerPackage, AdditionalLockerDelivererInfo additionalDelivererInfo) {
        return new PackageInfo(
                lockerPackage.getMainPackage(),
                lockerPackage.getPrice(),
                lockerPackage.getPredictionDeliveryDate(),
                additionalDelivererInfo
        );
    }

    private Integer id;
    private TypedAddress sourceAddress;
    private TypedAddress destinationAddress;
    private PackageSize packageSize;
    private LocalDateTime sendingDate;
    private LocalDateTime predictionDeliveryDate;
    private BigDecimal price;
    private AdditionalDelivererInfo additionalDelivererInfo;
}
