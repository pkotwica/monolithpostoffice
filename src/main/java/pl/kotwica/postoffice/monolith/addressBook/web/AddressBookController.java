package pl.kotwica.postoffice.monolith.addressBook.web;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kotwica.postoffice.monolith.addressBook.model.AddressDto;
import pl.kotwica.postoffice.monolith.addressBook.service.AddressBookService;
import pl.kotwica.postoffice.monolith.common.web.CommonRequestResponse;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/address-book")
public class AddressBookController {
    private final AddressBookService addressBookService;

    public AddressBookController(AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }

    @GetMapping("get-addresses")
    public List<AddressDto> getUserAddresses() {
        return addressBookService.getUserAddresses();
    }

    @PostMapping("add")
    public ResponseEntity<CommonRequestResponse> getUserAddresses(@RequestBody @Valid ShipmentFormAddress shipmentFormAddress, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(CommonRequestResponse.of(false, "Not valid form", bindingResult));
        } else {
            return addressBookService.add(shipmentFormAddress) ?
                    ResponseEntity.ok().body(CommonRequestResponse.of(true, "Address added")) :
                    ResponseEntity.ok().body(CommonRequestResponse.of(false, "Something goes wrong"));
        }
    }

    @DeleteMapping("remove")
    public ResponseEntity getUserAddresses(@RequestParam Integer addressId) {
        addressBookService.remove(addressId);
        return ResponseEntity.ok().build();
    }
}
