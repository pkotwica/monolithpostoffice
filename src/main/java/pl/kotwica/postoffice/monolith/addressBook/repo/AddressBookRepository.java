package pl.kotwica.postoffice.monolith.addressBook.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.user.model.User;

import java.util.List;

@Repository
public interface AddressBookRepository extends JpaRepository<Address, Integer> {
    List<Address> findAllByUser_Email(String email);
    boolean existsAddressByUserAndId(User user, Integer id);
}
