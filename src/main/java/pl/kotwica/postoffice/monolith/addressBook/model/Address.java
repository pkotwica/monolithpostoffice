package pl.kotwica.postoffice.monolith.addressBook.model;

import lombok.*;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;
import pl.kotwica.postoffice.monolith.user.model.User;

import javax.persistence.*;

@Entity(name = "addresses")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

    private Address(ShipmentFormAddress shipmentFormAddress, User user) {
        this.firstName = shipmentFormAddress.getFirstName();
        this.lastName = shipmentFormAddress.getLastName();
        this.country = shipmentFormAddress.getCountry();
        this.city = shipmentFormAddress.getCity();
        this.postcode = shipmentFormAddress.getPostcode();
        this.road = shipmentFormAddress.getRoad();
        this.number = shipmentFormAddress.getNumber();
        this.user = user;
    }

    public static Address of(ShipmentFormAddress shipmentFormAddress, User user) {
        return new Address(shipmentFormAddress, user);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String firstName;
    private String lastName;
    private String country;
    private String city;
    private String postcode;
    private String road;
    private Integer number;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
