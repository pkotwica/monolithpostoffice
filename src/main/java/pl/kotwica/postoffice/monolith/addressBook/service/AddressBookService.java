package pl.kotwica.postoffice.monolith.addressBook.service;

import org.springframework.stereotype.Service;
import pl.kotwica.postoffice.monolith.addressBook.model.Address;
import pl.kotwica.postoffice.monolith.addressBook.model.AddressDto;
import pl.kotwica.postoffice.monolith.addressBook.repo.AddressBookRepository;
import pl.kotwica.postoffice.monolith.packages.model.ShipmentFormAddress;
import pl.kotwica.postoffice.monolith.user.model.User;
import pl.kotwica.postoffice.monolith.user.service.UtilUserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AddressBookService {
    private final AddressBookRepository addressBookRepository;
    private final UtilUserService utilUserService;

    public AddressBookService(AddressBookRepository addressBookRepository, UtilUserService utilUserService) {
        this.addressBookRepository = addressBookRepository;
        this.utilUserService = utilUserService;
    }

    public List<AddressDto> getUserAddresses() {
        return addressBookRepository.findAllByUser_Email(utilUserService.getLoggedUserEmail()).stream()
                .map(AddressDto::of)
                .collect(Collectors.toList());
    }

    public boolean add(ShipmentFormAddress shipmentFormAddress) {
        Optional<User> loggedUserOpt = utilUserService.getLoggedUser();
        loggedUserOpt.ifPresent(u -> addressBookRepository.save(Address.of(shipmentFormAddress, u)));

        return loggedUserOpt.isPresent();
    }

    public void remove(Integer addressId) {
        final Optional<User> loggedUser = utilUserService.getLoggedUser();
        loggedUser.ifPresent(u -> {
            if(addressBookRepository.existsAddressByUserAndId(u, addressId)) {
                addressBookRepository.deleteById(addressId);
            }
        });
    }
}
