package pl.kotwica.postoffice.monolith.addressBook.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
    public static AddressDto of(Address address) {
        return new AddressDto(
                address.getId(),
                address.getFirstName(),
                address.getLastName(),
                address.getCountry(),
                address.getCity(),
                address.getPostcode(),
                address.getRoad(),
                address.getNumber()
        );
    }

    private Integer id;
    private String firstName;
    private String lastName;
    private String country;
    private String city;
    private String postcode;
    private String road;
    private Integer number;
}
